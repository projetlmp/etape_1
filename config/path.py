import os
import configparser

PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.join(os.path.abspath(__file__))))
test_data_dir_name = 'test_data'
name_entities_output_file = 'entities.json'
name_intermediate_entities_output_file = 'intermediate_entities.json'
path_source_keys = 'config/config_files'
path_google_cloud_cred = ""
data_elus_dir_name = "RNE"
data_assos_dir_name = "RNA"
Config = configparser.ConfigParser()
Config.read(os.path.join(PROJECT_PATH, 'config', 'config.ini'))
path_output_enriched_docs = Config.get("Paths", "path_output_enriched_docs")
path_input_docs = Config.get("Paths", "path_input_docs")
path_to_entities_dir = Config.get("Paths", "path_to_entities_dir")
path_to_schema = Config.get("Paths", "path_to_schema")




