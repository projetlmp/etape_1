xml_field_names = {
    "corps": "eddf:content",
    "id": "emd:identifier",
    "titre": "emd:title",
    "date": "emd:publicationdate",
    "sujet": "emd:theme",
    'source_code': 'emd:publisher',
}

xml_subject_codes = {
    "gspo" : "sport"
}
