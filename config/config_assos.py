rna_name = "rna_waldec_20180901.csv"

dict_rna_asso_name_to_used_asso_names = {
    "UNION REGIONALE DES ASSOCIATIONS DES COMMUNES FORESTIERES DE LORRAINE": ["réseau lorrain des communes forestières",
                                                                              "Union régionale des communes forestières de Lorraine"],
    "ASSOCIATION DES COMMUNES FORESTIERES VOSGIENNES": ["communes forestières vosgiennes",
                                                        "communes forestières des Vosges"],
    "ASSOCIATION DES COMMUNES FORESTIERES DU DEPARTEMENT DE LA MEUSE": ['communes forestières de la Meuse'],
    "ASSOCIATION DES COMMUNES FORESTIERES DE L'AUBE": ["communes forestières de l'Aube"],
}
