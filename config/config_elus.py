ordre_mandat = {u"Maire": 4, u"Membre du Conseil Municipal": 7, u"Président du groupement": 5, u"Membre de l'EPCI": 6,
                u"Député": 1, u"Sénateur": 0, u"Conseiller régional": 3,
                u"Conseiller départemental": 2}

loc_mandat = {u"Maire": {u'Région': lambda x: 0, u'Département': lambda x: x[0:2], u'Commune': lambda x: x},
              u"Membre du Conseil Municipal": {u'Région': lambda x: 0, u'Département': lambda x: x[0:2],
                                               u'Commune': lambda x: x},
              "Député": {u'Région': lambda x: 0, u'Département': lambda x: x, u'Commune': lambda x: 0},
              u"Président du groupement": {u'Région': lambda x: 0, u'Département': lambda x: x,
                                           u'Commune': lambda x: 0},
              u"Membre de l'EPCI": {u'Région': lambda x: 0, u'Département': lambda x: x, u'Commune': lambda x: 0},
              u"Sénateur": {u'Région': lambda x: 0, u'Département': lambda x: x, u'Commune': lambda x: 0},
              u"Conseiller régional": {u'Région': lambda x: 0, u'Département': lambda x: x, u'Commune': lambda x: 0},
              u"Conseiller départemental": {u'Région': lambda x: 0, u'Département': lambda x: x,
                                            u'Commune': lambda x: 0}
              }

config_mandat = {
    "CD.txt",
    "CM 01 50.txt",
    "CM 51 OM.txt",
    'CR.txt',
    'Deputes.txt',
    'EPCI.txt',
    "groupements.csv",
    "Maires.txt",
    "Senateurs.txt"
}

dict_mandat_loc = {
    "CD.txt": {"codes_regions": None, "codes_departements": (u"Code du département", lambda x: x),
               "codes_communes": None},
    "CM 01 50.txt": {"codes_regions": None, "codes_departements": (u"Code du département (Maire)", lambda x: x),
                     "codes_communes": (u"Code Insee de la commune", lambda x: x)},
    "CM 51 OM.txt": {"codes_regions": None, "codes_departements": (u"Code du département (Maire)", lambda x: x),
                     "codes_communes": (u"Code Insee de la commune", lambda x: x)},
    "CR.txt": {"codes_regions": (u"Code région", lambda x: x),
               "codes_departements": (u"Code du département", lambda x: x), "codes_communes": None},
    "Deputes.txt": {"codes_regions": None, "codes_departements": (u'Code du département', lambda x: x),
                    "codes_communes": None},
    "EPCI.txt": {"codes_regions": None, "codes_departements": (u'Code département EPCI', lambda x: x),
                 "codes_communes": ('Code de la commune', lambda x: x)},
    "groupements.csv": {"codes_regions": ('Région siège', lambda x: x.split(' ')[0]),
                        "codes_departements": (u"Département siège", lambda x: x.split(' ')[0]),
                        "codes_communes": None},
    "Maires.txt": {"codes_regions": None, "codes_departements": (u"Code du département (Maire)", lambda x: x),
                   "codes_communes": (u"Code Insee de la commune", lambda x: x)},
    "Senateurs.txt": {"codes_regions": None, "codes_departements": (u'Code du département', lambda x: x),
                      "codes_communes": None},
}

dict_mandat_elu_surname = {
    "CD.txt": u"Nom de l'élu",
    "CM 01 50.txt": u"Nom de l'élu",
    "CM 51 OM.txt": u"Nom de l'élu",
    "CR.txt": u"Nom de l'élu",
    "Deputes.txt": u"Nom de l'élu",
    "EPCI.txt": u"Nom de l'élu",
    "groupements.csv": u"Nom Président",
    "Maires.txt": u"Nom de l'élu",
    "Senateurs.txt": u"Nom de l'élu"
}

dict_mandat_elu_name = {
    "CD.txt": u"Prénom de l'élu",
    "CM 01 50.txt": u"Prénom de l'élu",
    "CM 51 OM.txt": u"Prénom de l'élu",
    "CR.txt": u"Prénom de l'élu",
    "Deputes.txt": u"Prénom de l'élu",
    "EPCI.txt": u"Prénom de l'élu",
    "groupements.csv": u"Prénom Président",
    "Maires.txt": u"Prénom de l'élu",
    "Senateurs.txt": u"Prénom de l'élu"
}

dict_mandat_nuance_politique = {
    "CD.txt": "Nuance politique (C. Gén.)",
    "CM 01 50.txt": "Nuance politique (C. Mun.)",
    "CM 51 OM.txt": "Nuance politique (C. Mun.)",
    "CR.txt": "Nuance mandat",
    "Deputes.txt": "Nuance politique (Député)",
    "EPCI.txt": "Nuance mandat",
    "Maires.txt": u"Nuance politique (C. Mun.)",
    "Senateurs.txt": u"Nuance politique (Sénateur)"
}
dict_mandat_elu_id = {
    "CD.txt": "N° Identification d'un élu",
    "CM 01 50.txt": u"N° Identification d'un élu",
    "CM 51 OM.txt": u"N° Identification d'un élu",
    "CR.txt": u"N° Identification d'un élu",
    "Deputes.txt": u"N° Identification d'un élu",
    "EPCI.txt": u"N° Identification d'un élu",
    "Maires.txt": u"N° Identification d'un élu",
    "Senateurs.txt": u"N° Identification d'un élu",
}

dict_mandat_elu_fonction = {
    "CD.txt": u"Libellé de fonction",
    "CM 01 50.txt": u"Libellé de fonction",
    "CM 51 OM.txt": u"Libellé de fonction",
    "CR.txt": u"Libellé de fonction",
    "Deputes.txt": u"Libellé de fonction",
    "EPCI.txt": u"Libellé de fonction",
}

dict_mandat_nom_loc = {
    "Maires.txt": u"Libellé de la commune",
    "CD.txt": u"Libellé du département",
    "CM 01 50.txt": u"Libellé de la commune",
    "CM 51 OM.txt": u"Libellé de la commune",
    "CR.txt": u"Libellé de la région",
    "Deputes.txt": u"Libellé du département",
    "EPCI.txt": u"Libellé de l'EPCI",
    "groupements.csv": u"Nom du groupement",
    "Senateurs.txt": u"Libellé du département"
}
