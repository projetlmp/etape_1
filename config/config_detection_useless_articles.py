details_regex = ['Renseignements', 'renseignement']
horaires_regex = ['Horaire', 'horaire', '\d\d?.?heure', '\d\d?.?h ', '\d\d?.?h.?\d\d']
other_regex = '\d{2}.?\d{2}.?\d{2}.?\d{2}'
mail_regex = '[^@]+@[^@]+\.[^@]+'
