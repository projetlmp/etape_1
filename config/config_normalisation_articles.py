from config.config_dow_jones import dj_field_names
from config.config_dow_jones import subject_codes
from config.config_scrapping import scrap_field_names
from config.config_scrapping import scrap_subject_code
from config.config_edd import xml_field_names
from config.config_edd import xml_subject_codes


dic_origin_to_source_dic_col = {
    'DJ': 'DJ Source Code',
    'EDD': 'EDD Source Code',
    'Scrap': 'Scrap Source Code'
}

dic_to_dic_source_fields = {
    'DJ': dj_field_names,
    'EDD': xml_field_names,
    'Scrap': scrap_field_names
}

dic_to_dic_subject_fields = {
    'DJ': subject_codes,
    'EDD': xml_subject_codes,
    'Scrap': scrap_subject_code
}