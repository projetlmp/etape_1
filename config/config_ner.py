# coding: utf-8

entity_types = {
    'inconnu': 'UNKNOWN',
    'personne': 'PERS',
    'lieu': 'LOC',
    'organisation': 'ORG',
    'evenement': 'EVENT',
    'work of art': 'WOA',
    'bien de consommation': 'CONSUMER_GOOD',
    'autre': 'OTHER'
}

google_entity_type_to_entity_type = {
    'UNKNOWN': entity_types['inconnu'],
    'PERSON': entity_types['personne'],
    'LOCATION': entity_types['lieu'],
    'ORGANIZATION': entity_types['organisation'],
    'EVENT': entity_types['evenement'],
    'WORK_OF_ART': entity_types['work of art'],
    'CONSUMER_GOOD': entity_types['bien de consommation'],
    'OTHER': entity_types['autre']
}

spacy_entity_type_to_entity_type = {
    'PER': entity_types['personne'],
    'MISC': entity_types['autre'],
    'ORG': entity_types['organisation'],
    'LOC': entity_types['lieu']
}

dict_label_package_to_lmp = {
    "spacy": spacy_entity_type_to_entity_type,
    "google": google_entity_type_to_entity_type
}