import math


class Mandat:
    def __init__(self, dict_loc, nom_loc, fonction, couleur_politique):
        self.dict_loc = dict_loc
        self.nom_loc = nom_loc
        self.fonction = fonction
        self.couleur_politique = couleur_politique

    def __to_dict__(self):
        return {
            attr: str(getattr(self, attr)) if not (
                    type(getattr(self, attr)) == float and math.isnan(getattr(self, attr))) else '' for
            attr in
            [a for a in dir(self) if not a.startswith('__')]}
