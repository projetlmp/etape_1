from config.config_ner import google_entity_type_to_entity_type
from utils.utils_entities_google import client_instantiation, get_google_entities_from_text, google_entity_type


class GoogleNer:
    def __init__(self, text, google_client=client_instantiation()):
        self.ner_results = get_google_entities_from_text(text, client=google_client)
