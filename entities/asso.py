import json
import os
from utils.path_generation import get_data_asso_path
from input.codes_localisations_of_interest import codes_localisation_of_interest
from utils.utils_for_text import to_standard

class Asso():
    def __init__(self, nom, id_loc, id, objet, name_loc, date_creat):
        self.nom = nom
        self.id_loc = id_loc
        self.id = id
        self.objet = objet
        self.name_loc = name_loc
        self.date_creat = date_creat

    def is_asso_in(self):
        if isinstance(self.id_loc, str):
            commune = self.id_loc
            codes_insee_commune = codes_localisation_of_interest["codes_insee_commune"]
            if commune in codes_insee_commune:
                return True
        return False

    def get_std_name(self):
        return to_standard(str(self.nom))

class AssosSet():
    def __init__(self, dict_assos):
        self.dict_assos = dict_assos

    def iter_assos(self):
        for id_asso in self.dict_assos:
            yield self.dict_assos[id_asso]

    def save(self, output_file='AssosSet.json'):
        json_file = {}
        for asso in self.iter_assos():
            id_asso = asso.id
            json_file[id_asso] = {attr: value for attr, value in asso.__dict__.items()}
        js = json.dumps(json_file)
        fp = open(os.path.join(get_data_asso_path(), output_file), 'w')
        fp.write(js)
        fp.close()

    def filter_by_loc(self):
        for id_asso in list(self.dict_assos):
            curr_asso = self.dict_assos[id_asso]
            if not curr_asso.is_elu_in():
                del self.dict_assos[id_asso]


