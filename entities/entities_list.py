from utils.utils import is_author


def add_type_to_entities_list(entities_list, doc_dict, dict_elus, dict_asso):
    """

    :param entities_list: let entities_set be an EntitiesSet, entities_list is entities_set.dict_entities[id_entity]['entities']
    :param doc_dict:
    :param overwrite:
    :param filter_by_loc:
    :return:
    """
    if not is_author(entities_list, doc_dict):
        for entity in entities_list:
            text_extract_containing_name = doc_dict[entity.doc]['corps']
            entity.check_if_person(text_extract_containing_name)
            if entity.person:
                entity.check_if_elu(dict_elus)
            if not entity.elu:
                entity.check_if_asso(dict_asso)


def entity_with_type_in_entities_list(entities_list):
    """

    :param entities_list: let entities_set be an EntitiesSet, entities_list is entities_set.dict_entities[id_entity]['entities']
    :return:
    """
    has_type = False
    for entity in entities_list:
        if entity.person or entity.elu or entity.asso:
            has_type = True
            break
    return has_type
