import json
import os

from text_toolkit.entity_names import name_is_too_small, is_person_name, find_complete_entity_name
from text_toolkit.is_public_place import is_public_place

from config.config_ner import entity_types
from config.path import path_to_entities_dir
from entities.entities_list import add_type_to_entities_list, entity_with_type_in_entities_list
from loaders.load_data_assos import get_dict_assos
from loaders.load_data_elus import get_dict_elus
from utils.path_generation import get_output_enriched_docs_path
from utils.utils import print_entities_for_selection, SetEncoder, format_dict_entities_for_reloading, \
    mandate_in_documents
from utils.utils_for_assos import get_map_used_asso_name_to_rna_asso_name
from utils.utils_for_contexts import get_contextes
from utils.utils_for_elus import ordre_mandat
from utils.utils_for_text import to_standard
from utils.verify_output_values_before_saving import verify_output_values_before_saving


class Entity:
    def __init__(self, id, name, label, doc, start_char, end_char, asso=False, elu=False, person=False):
        self.id = id
        self.name = name
        self.doc = doc
        self.label = label
        self.start_char = start_char
        self.end_char = end_char
        self.asso = asso
        self.elu = elu
        self.person = person

    def at_end_or_beg_of_doc(self, text, dist_to_end_and_begining_of_doc=10):
        return (len(text) - self.end_char <= dist_to_end_and_begining_of_doc) or (
                self.start_char <= dist_to_end_and_begining_of_doc)

    def is_photograph(self):
        return 'photo' in to_standard(self.name, keep_spaces=True)

    def check_if_person(self, text_extract_containing_name):
        if (entity_types["personne"] == self.label) and (is_person_name(self.name)) and not (
                is_public_place(self.name, text_extract_containing_name)) and not (self.is_photograph()):
            self.person = True

    def check_if_elu(self, filtered_dict_elus):
        """
        :param filtered_dict_elus:
        :return: True if the entity is elected, False otherwise. In this case, the entity standard name will not be in
         the set of elected people and their will be a KeyError.
        """
        std_name = to_standard(self.name)
        try:
            elus_list_with_same_std_name = filtered_dict_elus[std_name]
            self.elu = [elu_id for elu_id in elus_list_with_same_std_name]
        except KeyError:
            pass

    def check_if_asso(self, dict_assos):
        """
        :param optimised_elus_dict:
        :return: True if the entity is an association, False otherwise. In this case, the entity standard name will not
        be in the RNA and their will be a KeyError.
        """
        map_used_asso_name_to_rna_asso_name = get_map_used_asso_name_to_rna_asso_name()
        std_name = to_standard(self.id, map_used_asso_name_to_rna_asso_name=map_used_asso_name_to_rna_asso_name)
        try:
            assos_list_with_same_std_name = dict_assos[std_name]
            self.asso = [asso_id for asso_id in assos_list_with_same_std_name]
        except KeyError:
            pass

    def format_entity_for_json(self):
        return {attribute_name: self.__getattribute__(attribute_name) for attribute_name in self.__dict__ if
                attribute_name[0:2] != '__'}


class EntitiesSet:
    def __init__(self, dict_entities, pre_loaded_path=False):
        if pre_loaded_path:
            with open(pre_loaded_path) as f:
                data = json.load(f)
            self.dict_entities = {
                entity_id: {"entities": [Entity(**e) for e in data[entity_id]['entities']],
                            "pertinence": data[entity_id]['pertinence'],
                            "importance": data[entity_id]['importance']} for entity_id in data}
        else:
            self.dict_entities = dict_entities

    def clean_entities_name(self, corpus):
        for id_entity in list(self.dict_entities):
            for entity in self.dict_entities[id_entity]['entities']:
                if name_is_too_small(entity.name):
                    del self.dict_entities[id_entity]
                    break
                else:
                    entity.name = find_complete_entity_name(entity.name, entity.start_char, corpus[entity.doc]['corps'])

    def filter_entities_by_location(self, corpus):
        for id_entity in list(self.dict_entities):
            if len([loc for entity in self.dict_entities[id_entity]['entities'] for loc in
                    corpus[entity.doc]['locations']]) == 0:
                del self.dict_entities[id_entity]

    def add_pertinence(self, corpus):
        for id_entity in list(self.dict_entities):
            doc_ids = set([entity.doc for entity in self.dict_entities[id_entity]['entities']])
            pertinence = sum([1 if corpus[id]['themes'] != [] else 0 for id in doc_ids])
            self.dict_entities[id_entity]['pertinence'] = pertinence

    def add_importance(self):
        for id_entity in list(self.dict_entities):
            doc_ids = set([entity.doc for entity in self.dict_entities[id_entity]['entities']])
            self.dict_entities[id_entity]['importance'] = len(doc_ids)

    def add_type(self, doc_dict, overwrite, filter_by_loc=True):
        filtered_dict_elus = get_dict_elus(overwrite, filter_by_loc=filter_by_loc)
        filtered_dict_asso = get_dict_assos(overwrite, filter_by_loc=filter_by_loc)
        for id_entity in list(self.dict_entities):
            add_type_to_entities_list(self.dict_entities[id_entity]['entities'],
                                      doc_dict,
                                      dict_elus=filtered_dict_elus,
                                      dict_asso=filtered_dict_asso)

    def remove_entities_with_no_type(self):
        for id_entity in list(self.dict_entities):
            if not entity_with_type_in_entities_list(self.dict_entities[id_entity]):
                del self.dict_entities[id_entity]

    def select_relevant_entities(self, corpus,
                                 dict_elus,
                                 dict_assos,
                                 nb_to_keep=2,
                                 manual_selection=True,
                                 sort_by_pertinence=False,
                                 keep_only_persons=True,
                                 keep_all_elu=False):
        kept = 0
        try:
            with open(os.path.join(get_output_enriched_docs_path('selected_dict_entities.json'))) as f:
                output_entities = json.load(f)
            kept = len(output_entities)
        except FileNotFoundError:
            output_entities = {}
        try:
            with open(os.path.join(get_output_enriched_docs_path('rejected_dict_entities.json'))) as f:
                junk_entities = json.load(f)
        except FileNotFoundError:
            junk_entities = {}
        key = 'importance'
        if sort_by_pertinence:
            key = 'pertinence'
        if manual_selection:
            for id_entity, value in sorted(self.dict_entities.items(), key=lambda x: x[1][key], reverse=True):
                done = False
                try:
                    if junk_entities[id_entity]:
                        done = True
                except KeyError:
                    pass
                try:
                    if output_entities[id_entity]:
                        done = True
                except KeyError:
                    pass
                if not done:
                    add_type_to_entities_list(self.dict_entities[id_entity]['entities'], corpus, dict_elus, dict_assos)
                    has_past_mandate = mandate_in_documents(self.dict_entities[id_entity]['entities'], corpus) and not \
                        self.dict_entities[id_entity]['entities'][0].elu
                    if keep_only_persons and self.dict_entities[id_entity]['entities'][
                        0].person and not has_past_mandate:
                        if entity_with_type_in_entities_list(self.dict_entities[id_entity]['entities']):
                            print_entities_for_selection(self.dict_entities[id_entity]['entities'], corpus)
                            print(self.dict_entities[id_entity][key])
                            print(self.dict_entities[id_entity]['entities'][0].elu)
                            if keep_all_elu & isinstance(self.dict_entities[id_entity]['entities'][0].elu, list):
                                output_entities[id_entity] = output_entities[id_entity] = {'entities': [
                                    curr_ent.format_entity_for_json() for curr_ent in
                                    self.dict_entities[id_entity]['entities']],
                                    'pertinence': self.dict_entities[id_entity]["pertinence"],
                                    "importance": self.dict_entities[id_entity]["importance"]}
                                kept += 1
                                print('%s / %s' % (kept, nb_to_keep))
                                js = json.dumps(output_entities, cls=SetEncoder)
                                fp = open(os.path.join(get_output_enriched_docs_path('selected_dict_entities.json')),
                                          'w+')
                                fp.write(js)
                                fp.close()
                            else:
                                to_keep = input('Type Y if you want to keep << %s >>.' % id_entity)
                                if to_keep in ('Y', 'y'):
                                    output_entities[id_entity] = {'entities': [
                                        curr_ent.format_entity_for_json() for curr_ent in
                                        self.dict_entities[id_entity]['entities']],
                                        'pertinence': self.dict_entities[id_entity]["pertinence"],
                                        "importance": self.dict_entities[id_entity]["importance"]}
                                    kept += 1
                                    print('%s / %s' % (kept, nb_to_keep))
                                    js = json.dumps(output_entities, cls=SetEncoder)
                                    fp = open(
                                        os.path.join(get_output_enriched_docs_path('selected_dict_entities.json')),
                                        'w+')
                                    fp.write(js)
                                    fp.close()
                                else:
                                    junk_entities[id_entity] = {'entities': [
                                        curr_ent.format_entity_for_json() for curr_ent in
                                        self.dict_entities[id_entity]['entities']],
                                        'pertinence': self.dict_entities[id_entity]["pertinence"],
                                        "importance": self.dict_entities[id_entity]["importance"]}
                                    js = json.dumps(junk_entities, cls=SetEncoder)
                                    fp = open(
                                        os.path.join(get_output_enriched_docs_path('rejected_dict_entities.json')),
                                        'w+')
                                    fp.write(js)
                                    fp.close()
                            if kept == nb_to_keep:
                                break
            else:
                counter = 0
                for id_entity, value in sorted(self.dict_entities.items(), key=lambda x: x[1][key], reverse=True):
                    add_type_to_entities_list(self.dict_entities[id_entity]['entities'], corpus, dict_elus, dict_assos)
                    if entity_with_type_in_entities_list(self.dict_entities[id_entity]['entities']):
                        output_entities = {id_entity: self.dict_entities[id_entity]}
                        counter += 1
                    if counter == nb_to_keep:
                        break
        self.dict_entities = {id: self.dict_entities[id] for id in output_entities}


    def add_contextes(self, corpus):
        for id_entity in self.dict_entities:
            self.dict_entities[id_entity]['contextes'] = get_contextes(self.dict_entities[id_entity][
                                                                           'entities'], corpus,
                                                                       sentences_nb=1)

    def save_elu(self, std_name, entity_attributes, dict_elus):
        all_mandates = [curr_mandat for curr_elu_id in dict_elus[std_name] for curr_mandat in
                        dict_elus[std_name][curr_elu_id].mandats]
        mandats = sorted(all_mandates, key=lambda x: ordre_mandat[x.fonction])
        entity_attributes['mandates'] = []
        for i, mandat in enumerate(mandats):
            entity_attributes['mandates'].append(mandat.__to_dict__())
        return entity_attributes

    def save_asso(self, std_name, entity_attributes, dict_assos):
        id_asso = list(dict_assos[std_name].keys())[0]
        entity_attributes['association_description'] = dict_assos[std_name][id_asso].objet
        entity_attributes['name_loc_asso'] = dict_assos[std_name][id_asso].name_loc
        entity_attributes['date_creat_asso'] = dict_assos[std_name][id_asso].date_creat
        return entity_attributes

    def save_for_dev(self, corpus, filename='entities.json', overwrite=False, path=None):
        to_save = []
        dict_elus = get_dict_elus(overwrite)
        dict_assos = get_dict_assos(overwrite)
        for id_entity in self.dict_entities:
            name = self.dict_entities[id_entity]['entities'][0].name
            docs = set([e.doc for e in self.dict_entities[id_entity]['entities'] if e.doc in corpus])
            themes = set(
                [theme for list_themes in [corpus[id_doc]['themes'] for id_doc in docs] for theme in list_themes])
            themes = [{'name': curr_theme} for curr_theme in themes]
            locations = set(
                [loc for list_location in [corpus[id_doc]['locations'] for id_doc in docs] for loc in
                 list_location])
            locations = [{'name': loc} for loc in locations]
            list_id_elu = [e.elu for e in self.dict_entities[id_entity]['entities'] if e.elu]
            is_elu = len(list_id_elu) > 0
            list_id_asso = [e.asso for e in self.dict_entities[id_entity]['entities'] if e.asso]
            is_association = (not is_elu) and (len(list_id_asso) > 0)
            is_other = (not is_elu) and (not is_association)
            contextes = self.dict_entities[id_entity]['contextes']
            total_mentions = len(contextes)
            pertinence = len([c for c in contextes if c['themes'] != []])
            entity_attributes = {"id": id_entity, "name": name, "pertinence": pertinence,
                                 "total_mentions": total_mentions,
                                 "themes": themes, "is_elu": is_elu, "is_association": is_association,
                                 "is_other": is_other,
                                 "contextes": contextes, "locations": locations}
            if is_elu:
                std_name = to_standard(name)
                entity_attributes = self.save_elu(std_name, entity_attributes, dict_elus)
            elif is_association:
                std_name = to_standard(name)
                entity_attributes = self.save_asso(std_name, entity_attributes, dict_assos)
            to_save.append(entity_attributes)
        to_save = sorted(to_save, key=lambda x: x['total_mentions'], reverse=True)
        assert (verify_output_values_before_saving(to_save))
        js = json.dumps(to_save, cls=SetEncoder)

        if path:
            path_to_save = path
        else:
            path_to_save = path_to_entities_dir
        if not os.path.isdir(path_to_save):
            os.makedirs(path_to_save)

        fp = open(os.path.join(path_to_save, '%s' % filename), 'w')
        fp.write(js)
        fp.close()
        print('entities saved at {}'.format(os.path.join(path_to_save, '%s' % filename)))

    def save_entities_sets_for_reloading(self, directory_name, filename):
        to_save = format_dict_entities_for_reloading(self.dict_entities)
        js = json.dumps(to_save, cls=SetEncoder)
        path = os.path.join(directory_name, filename)
        if not os.path.isdir(directory_name):
            os.makedirs(directory_name)
        fp = open(path, 'w')
        fp.write(js)
        fp.close()

    def do_all_enrichments(self, doc_dict, filter_by_loc=True):
        """
        :param doc_dict:
        :return: self.dict_entities is cleaned from non relevant entities ; 'pertinence' and 'importance' keys are added
        to self.dict_entities[ent_id] for each ent_id ; 'elu', 'asso' or 'person' tag is added to each entity in self.dict_entities[ent_id] ;
        entities with no type are deleted.
        """
        if filter_by_loc:
            print("Filter entities by locations")
            self.filter_entities_by_location(doc_dict)
        print("Clean names")
        self.clean_entities_name(doc_dict)
        print("Add pertinence")
        self.add_pertinence(doc_dict)
        print("Add relevance")
        self.add_importance()

    def manage_homonymes(self):
        for id_entity in self.dict_entities:
            list_entities = []
            for curr_ent in self.dict_entities[id_entity]['entities']:
                if curr_ent.elu:
                    list_elu = curr_ent.elu
                    for elu_id in list_elu:
                        new_ent = Entity(curr_ent.id, curr_ent.name, curr_ent.label, curr_ent.doc, curr_ent.start_char,
                                         curr_ent.end_char, curr_ent.asso, [elu_id], curr_ent.person)
                        list_entities.append(new_ent)
                elif curr_ent.asso:
                    list_asso = curr_ent.asso
                    for asso_id in list_asso:
                        new_ent = Entity(curr_ent.id, curr_ent.name, curr_ent.label, curr_ent.doc, curr_ent.start_char,
                                         curr_ent.end_char, [asso_id], curr_ent.elu, curr_ent.person)
                        list_entities.append(new_ent)
                else:
                    list_entities.append(curr_ent)
            self.dict_entities[id_entity]['entities'] = list_entities
