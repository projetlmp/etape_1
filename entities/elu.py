import json
import os
from config.config_elus import ordre_mandat, loc_mandat
from input.codes_localisations_of_interest import codes_localisation_of_interest
from utils.path_generation import get_data_elus_path
from utils.utils_for_text import to_standard


class Elu():
    def __init__(self, nom, prenom, mandat, id):
        self.nom = nom
        self.prenom = prenom
        self.id = id
        if isinstance(mandat, list):
            self.mandats = mandat
        else:
            self.mandats = [mandat]

    def is_elu_in(self, codes_insee_commune, codes_departement, codes_region):
        for curr_mandat in self.mandats:
            region = curr_mandat.dict_loc["codes_regions"]
            departement = curr_mandat.dict_loc["codes_departements"]
            commune = curr_mandat.dict_loc["codes_communes"]
            departement_from_codes_insee_commune = list(map(lambda x: x[:-3], codes_insee_commune))
            if region in codes_region or commune in codes_insee_commune or departement in departement_from_codes_insee_commune + codes_departement:
                return True
        return False

    def get_std_name(self):
        return to_standard(str(self.prenom) + str(self.nom))


class ElusSet():
    def __init__(self, dict_elus):
        self.dict_elus = dict_elus

    def iter_elus(self):
        for id_elu in self.dict_elus:
            yield self.dict_elus[id_elu]

    def merge_with(self, elusset):
        """
        merge mandats of two Elus from self and another ElusSet if they have the same id
        :param elusset:
        :return:
        """
        for elu_to_merge in elusset.iter_elus():
            try:
                self.dict_elus[elu_to_merge.id].mandats = elu_to_merge.mandats + self.dict_elus[elu_to_merge.id].mandats
            except KeyError:
                self.dict_elus[elu_to_merge.id] = elu_to_merge

    def save(self, output_file='ElusSet.json'):
        json_file = {}
        for elu in self.iter_elus():
            id_elu = str(elu.id)
            json_file[id_elu] = {}
            mandats = sorted(elu.mandats, key=lambda x: ordre_mandat[x.fonction])
            json_file[id_elu]['mandats'] = {'mandat_%s' % i: mandat.__to_dict__() for i, mandat in
                                            enumerate(mandats)}
            json_file[id_elu]['nom'] = elu.nom
            json_file[id_elu]['prenom'] = elu.prenom
            json_file[id_elu]['id'] = id_elu
        js = json.dumps(json_file)
        fp = open(os.path.join(get_data_elus_path(), output_file), 'w')
        fp.write(js)
        fp.close()

    def filter_by_loc(self):
        codes_insee_commune = codes_localisation_of_interest["codes_insee_commune"]
        codes_departement = codes_localisation_of_interest["codes_departement"]
        codes_region = codes_localisation_of_interest["codes_region"]
        for id_elu in list(self.dict_elus):
            curr_elu = self.dict_elus[id_elu]
            if not curr_elu.is_elu_in(codes_insee_commune, codes_departement, codes_region):
                del self.dict_elus[id_elu]
