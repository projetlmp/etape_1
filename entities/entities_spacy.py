import fr_core_news_md
nlp = fr_core_news_md.load()


class SpacyNer:
    def __init__(self, text):
        self.ner_results = nlp(text).ents
