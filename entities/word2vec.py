# coding: utf-8
import numpy as np
from utils.path_generation import get_external_data_path



class Word2vec():
    def __init__(self, fname, nmax=100000):
        self.word2vec = {}
        self.load_wordvec(fname, nmax)

    def load_wordvec(self, fname, nmax):
        with open(fname) as f:
            next(f)
            for i, line in enumerate(f):
                word, vec = line.split(' ', 1)
                self.word2vec[word] = np.fromstring(vec, sep=' ')
                if i == (nmax - 1):
                    break

    def score(self, vect_1, vect_2):
        return np.dot(vect_1, vect_2) / (np.linalg.norm(vect_1) * np.linalg.norm(vect_2))


w2v = Word2vec(get_external_data_path('wiki.fr.vec'), nmax=500000)
