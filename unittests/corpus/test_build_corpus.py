import unittest
from utils.utils_for_corpus import build_corpus

class TestCorpus(unittest.TestCase):
    def setUp(self):
        corpus_path = "../../input/test_data/HUMAN"
        corpus_source = "DJ"
        corpus = build_corpus(corpus_source, corpus_path)
        self.doc = corpus[0]


    def test_doc_keys(self):
        self.assertTrue(all([key in list(self.doc.keys()) for key in ['id', 'date', 'corps', 'titre', 'source_code']]))
