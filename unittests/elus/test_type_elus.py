import os
import unittest
from entities.entities import EntitiesSet
from loaders.corpus_normalizer import CorpusSourceNormalizer
from utils.path_generation import get_path_test_articles
from utils.utils_for_corpus import get_entities_from_list_of_doc, read_corpus
from utils.utils_for_doc import build_doc, enrich_doc

corpus_path = os.path.join(get_path_test_articles(), 'dj')


class TestAddType(unittest.TestCase):
    def setUp(self):
        source_normalizer = CorpusSourceNormalizer(corpus_source='DJ')
        built_corpus = [build_doc(doc, source_normalizer) for doc in read_corpus(corpus_path)]
        enriched_corpus = [enrich_doc(doc) for doc in built_corpus]
        self.entities_set = EntitiesSet(dict_entities=get_entities_from_list_of_doc(enriched_corpus, package="spacy"))

    def test_add_type_person_to_entity(self):
        self.assertTrue(
            [ent.check_if_person() for ent in self.entities_set.dict_entities['michellegouge']['entities']] == [True, True])
