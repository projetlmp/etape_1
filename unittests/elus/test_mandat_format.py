import unittest
from loaders.load_data_elus import load_data_elus, elus_per_mandate_generator



class TestMandatFormat(unittest.TestCase):
    def setUp(self):
        mandate = 'groupements.csv'
        data = load_data_elus(mandate)
        elus = [elu for elu in elus_per_mandate_generator(data, mandate, filter_by_loc=False)]
        dict_elus = {elu.id: elu for elu in elus}
        self.mandat_to_check = dict_elus['PRESJeanDEGUERRY'].mandats[0]

    def test_mandat_attribute_values(self):
        self.assertTrue(self.mandat_to_check.dict_loc == {'codes_regions': '84', 'codes_departements': '01', 'codes_communes': -1})
        self.assertTrue(self.mandat_to_check.fonction == 'Président du groupement')
        self.assertTrue(self.mandat_to_check.nom_loc == 'Haut - Bugey Agglomération')
        self.assertTrue(self.mandat_to_check.couleur_politique == 'Pas de nuance politique')
