import unittest
from loaders.load_data_elus import load_data_elus, elus_per_mandate_generator


class TestElusLoc(unittest.TestCase):
    def setUp(self):
        mandate = 'groupements.csv'
        data = load_data_elus(mandate)
        elus = [elu for elu in elus_per_mandate_generator(data, mandate, filter_by_loc=False)]
        dict_elus = {elu.id: elu for elu in elus}
        self.elu_to_check = dict_elus['PRESJeanDEGUERRY']

    def test_mandat_attribute_values(self):
        self.assertTrue(self.elu_to_check.is_elu_in(codes_insee_commune=[], codes_departement=['01'], codes_region=[]))
        self.assertTrue(self.elu_to_check.is_elu_in(codes_insee_commune=[], codes_departement=[], codes_region=['84']))
        self.assertFalse(self.elu_to_check.is_elu_in(['75111'], [], []))
