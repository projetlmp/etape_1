import json
import os
import shutil
import unittest
from loaders.get_ner import generate_and_process_entities_set
from utils.utils_for_corpus import build_corpus, load_json_saved_corpus, get_entities_from_list_of_doc
from utils.utils_for_doc import get_doc_topics


class TestPipeline(unittest.TestCase):
    def setUp(self):
        with open("./../../input/list_locations_of_interest.txt", 'w') as f:
            f.write("Paris")
        with open("./../../input/codes_insee", 'w') as f:
            f.write("75000")
        self.corpus_path = "./../../input/test_data/HUMAN"
        self.output_temp = "./../../temp/"
        self.corpus_source = "DJ"
        self.output_expected_value = [
            {'id': 'annehidalgo', 'name': 'Anne Hidalgo', 'pertinence': 0, 'total_mentions': 1, 'themes': [],
             'is_elu': True, 'is_association': False, 'is_other': False, 'contextes': [{'title': 'exemple ',
                                                                                        'text': 'Bonjour je suis Anne Hidalgo la maire de Paris.  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
                                                                                        'source': 'HUMAN',
                                                                                        'date': '2008-03-11',
                                                                                        'themes': [], 'locations': [
                    {'name': 'Paris'}], 'influenceurs': [{'name': 'Anne Hidalgo'}]}], 'locations': [{'name': 'Paris'}],
             'mandates': [{'couleur_politique': 'SOC',
                           'dict_loc': "{'codes_regions': -1, 'codes_departements': '75', 'codes_communes': '056'}",
                           'fonction': 'Maire', 'nom_loc': 'Paris'}, {'couleur_politique': 'SOC',
                                                                      'dict_loc': "{'codes_regions': -1, 'codes_departements': '75', 'codes_communes': '056'}",
                                                                      'fonction': "Membre de l'EPCI",
                                                                      'nom_loc': 'METROPOLE DU GRAND PARIS'},
                          {'couleur_politique': 'SOC',
                           'dict_loc': "{'codes_regions': -1, 'codes_departements': '75', 'codes_communes': '056'}",
                           'fonction': 'Membre du Conseil Municipal', 'nom_loc': 'Paris'}]}]

    def test_build_corpus(self):
        build_corpus(self.corpus_source, self.corpus_path, enriched_corpus_saving_path=self.output_temp)
        self.assertTrue(os.path.isfile(os.path.join(self.output_temp, 'HUMAN/2008/03/HUMAN_20080311_9652921.json')))
        shutil.rmtree(os.path.join(self.output_temp, 'HUMAN'))

    def test_output_value(self):
        build_corpus(self.corpus_source, self.corpus_path, enriched_corpus_saving_path=self.output_temp)
        doc_list = [get_doc_topics(doc) for doc in load_json_saved_corpus(self.output_temp)]
        dict_entity_test = get_entities_from_list_of_doc(corpus=doc_list, package='spacy')
        generate_and_process_entities_set(entity_dict=dict_entity_test,
                                          doc_list=doc_list,
                                          overwrite=False,
                                          nb_entities=2,
                                          manual_selection=False,
                                          filename='entities_test.json',
                                          save_temp=False,
                                          load_from_temp=False,
                                          path_temp=self.output_temp,
                                          path_entities=self.output_temp)
        with open(os.path.join(self.output_temp, 'entities_test.json')) as f:
            output_value = json.load(f)
        self.assertEqual(self.output_expected_value[0]['id'], output_value[0]['id'])
        shutil.rmtree(os.path.join(self.output_temp, 'HUMAN'))
        os.remove(os.path.join(self.output_temp, 'entities_test.json'))
