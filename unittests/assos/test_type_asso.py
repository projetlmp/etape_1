import unittest
from entities.entities import EntitiesSet
from loaders.load_data_assos import get_dict_assos
from utils.utils_for_corpus import get_entities_from_list_of_doc
from utils.utils_for_doc import enrich_doc


class TestAddTypeAsso(unittest.TestCase):
    def setUp(self):
        with open("./../../input/codes_localisations_of_interest.py", 'w') as f:
            f.write(
                "codes_localisation_of_interest = {\"codes_departement\": [], \"codes_insee_commune\": [\"63113\"], \"codes_region\": []}")
        with open("./../../input/list_locations_of_interest.txt", 'w') as f:
            f.write("Reyrieux")

        corpus = [{'id': 'PROGRS0020131005e9a5000a9',
                   'date': '2013-10-05',
                   'corps': 'bla bla bla bla bla bla bla bla bla bla Black Owl est une super asso ! Nathalie Barde est la maire de Reyrieux, Véronique Baude est la maire de Divonne-les-Bains.Black Owl est une super asso ! Nathalie Barde est la maire de Reyrieux, Véronique Baude est la maire de Divonne-les-Bains.', },
                  {'id': 'PROGRS0020100303e63300007',
                   'date': '2010-03-03',
                       'corps': "Fini l'ouvrier du bâtiment de Reyrieux qui trimait des heures et des heures dans des conditions peu conformes au code du travail.Black Owl est une super asso ! Nathalie Barde est la maire de Reyrieux, Véronique Baude est la maire de Divonne-les-Bains."}]
        enriched_corpus = [enrich_doc(doc) for doc in corpus]
        entities_set = EntitiesSet(dict_entities=get_entities_from_list_of_doc(enriched_corpus, package="spacy"))
        doc_dict = {doc['id']: doc for doc in enriched_corpus}
        entities_set.add_type(doc_dict, overwrite=True, filter_by_loc=True)
        self.entities_set = entities_set
        self.loaded_asso_set = get_dict_assos(overwrite=False)

    def test_val_assos_set(self):
        self.assertTrue(list(self.loaded_asso_set['blackowl'].keys()) == ['W632007046', 'W632007519'])
        self.assertTrue(self.loaded_asso_set['blackowl']['W632007046'].name_loc == 'Clermont-Ferrand')
        self.assertTrue(self.loaded_asso_set['blackowl']['W632007046'].date_creat == '2014-06-17')

    def test_assos_detected(self):
        self.assertTrue(self.entities_set.dict_entities['blackowl']['entities'][0].asso == ['W632007046', 'W632007519'])
        pass
