import unittest
from entities.entities import EntitiesSet, Entity


class TestCleanEntities(unittest.TestCase):
    def setUp(self):
        ent_1 = Entity(id='ab', name='a b', label='OTHER', doc='id_doc', start_char=0, end_char=2)
        ent_2 = Entity(id='pierretruc', name='Pierre Truc', label='PERS', doc='id_doc', start_char=31, end_char=41)
        corpus = {"id_doc": {"id": "id_doc",
                              'date': '2013-10-05',
                              'titre': "coucou",
                              'source_code': "lol",
                              'corps': 'a b n\'est pas une entité. Jean-Pierre Truc si. ',
                              }
                   }
        entities_set = EntitiesSet(dict_entities={ent_1.id: {"entities": [ent_1], 'importance': 0, 'pertinence': 0},
                                                  ent_2.id: {"entities": [ent_2], 'importance': 0, 'pertinence': 0}})
        entities_set.clean_entities_name(corpus)
        self.entities_set = entities_set

    def test_remove_too_small_entities(self):
        self.assertTrue(list(self.entities_set.dict_entities.keys()) == ['pierretruc'])

    def test_complete_cut_names(self):
        self.assertTrue(self.entities_set.dict_entities['pierretruc']['entities'][0].name == 'Jean-Pierre Truc')
