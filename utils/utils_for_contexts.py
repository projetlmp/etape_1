from text_toolkit.split_into_sentences import split_into_sentences
from utils.utils_for_text import to_standard


def get_context_per_text(id_entity, text, sentences_nb):
    sentences = split_into_sentences(text)
    contextes = []
    done = set()
    for i in range(0, len(sentences)):
        c = []
        std_sentence = to_standard(sentences[i])
        if id_entity in std_sentence:
            if i not in done:
                for j in range(max(i - sentences_nb, 0), i):
                    if j not in done:
                        c.append(sentences[j])
                        done.add(j)
                done.add(i)
                c.append(sentences[i])
                for j in range(i + 1, min(i + sentences_nb + 1, len(sentences) - 1)):
                    if j not in done:
                        done.add(j)
                        c.append(sentences[j])
        if len(c) > 0:
            contextes.append(" ".join(c))
    return contextes


def format_contextes(list_contextes, corpus):
    formatted_contextes = []
    for curr_contexte in list_contextes:
        titre = corpus[curr_contexte['id_doc']]['titre']
        source = corpus[curr_contexte['id_doc']]['source_code']
        date = corpus[curr_contexte['id_doc']]['date']
        themes = [{"name": curr_theme} for curr_theme in corpus[curr_contexte['id_doc']]['themes']]
        location = [{"name": loc} for loc in corpus[curr_contexte['id_doc']]['locations']]
        influenceurs = corpus[curr_contexte['id_doc']]['influenceurs']
        formatted_contextes.append(
            {"title": titre, "text": curr_contexte["contextes"], "source": source, "date": date, "themes": themes,
             "locations": location, "influenceurs": influenceurs})
    return formatted_contextes


def sort_chronologically(contextes):
    return sorted(contextes, key=lambda x: str('').join(x['date'].split('-')), reverse=True)


def get_contextes(list_entities, corpus, sentences_nb):
    map_doc_to_entity_ids = {}
    for ent in list_entities:
        if ent.doc not in map_doc_to_entity_ids:
            map_doc_to_entity_ids[ent.doc] = {ent.id}
        else:
            map_doc_to_entity_ids[ent.doc].update({ent.id})
    list_contextes_per_doc = [
        {"contextes": get_context_per_text(id_ent, corpus[id_doc]['corps'], sentences_nb),
         "id_doc": id_doc} for id_doc in map_doc_to_entity_ids for id_ent in map_doc_to_entity_ids[id_doc] if id_doc in corpus]
    list_more_relevant_contextes = [{"contextes": choose_most_relevant_context_in_doc(contexts_in_a_doc["contextes"]),
                                     "id_doc": contexts_in_a_doc["id_doc"]}
                                    for contexts_in_a_doc in list_contextes_per_doc]
    formatted_contextes = format_contextes(list_more_relevant_contextes, corpus)
    chronologically_sorted_formatted_contextes = sort_chronologically(formatted_contextes)
    return chronologically_sorted_formatted_contextes


def choose_most_relevant_context_in_doc(contexts):
    if len(contexts) > 0:
        for curr_contexte in contexts:
            if '>>' in curr_contexte or '<<' in curr_contexte:
                return curr_contexte
        return contexts[-1]
    else:
        return contexts
