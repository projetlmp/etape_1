from config.config_ner import dict_label_package_to_lmp
from entities.entities import Entity
from utils.utils_for_assos import get_map_used_asso_name_to_rna_asso_name
from utils.utils_for_text import to_standard


def get_dict_entities_from_doc(doc, package):
    """
    doc is a dict from a single document. It should contains ner_results, text fields
    :param doc:
    :param package:
    :return:
    """
    dict_entities = {}
    map_used_asso_name_to_rna_asso_name = get_map_used_asso_name_to_rna_asso_name()
    if isinstance(doc, dict):
        for ent in doc['ner_results']:
            std_name = to_standard(ent['text'], map_used_asso_name_to_rna_asso_name=map_used_asso_name_to_rna_asso_name)
            if std_name not in dict_entities:
                dict_entities[std_name] = {'entities': [], 'importance': 0, 'pertinence': 0}
            lmp_label = dict_label_package_to_lmp[package][ent['label_']]
            dict_entities[std_name]['entities'].append(
                Entity(id=to_standard(ent['text']), name=ent['text'], label=lmp_label, doc=doc['id'],
                       start_char=ent['start_char'], end_char=ent['end_char']))
    else:
        print('something was wrong, the doc %s is not a dict, it is a %s' % (repr(doc), type(doc)))
        raise TypeError
    return dict_entities


def clean_location_entity_name(ner_result, location_name):
    ner_result['text'] = location_name
    return ner_result


def merge_dict_entities(doc_entities, dict_entities):
    for id_entity in doc_entities:
        if id_entity in dict_entities:
            dict_entities[id_entity]['entities'].extend(doc_entities[id_entity]['entities'])
        else:
            dict_entities[id_entity] = {}
            dict_entities[id_entity]['entities'] = doc_entities[id_entity]['entities']
            dict_entities[id_entity]['importance'] = 0
            dict_entities[id_entity]['pertinence'] = 0
    return dict_entities
