from config.config_assos import dict_rna_asso_name_to_used_asso_names
from utils.utils_for_text import to_standard

def get_map_used_asso_name_to_rna_asso_name():
    map_used_asso_name_to_rna_asso_name = {}
    for curr_rna_asso_name in dict_rna_asso_name_to_used_asso_names:
        for curr_used_asso_name in dict_rna_asso_name_to_used_asso_names[curr_rna_asso_name]:
            map_used_asso_name_to_rna_asso_name[to_standard(curr_used_asso_name)] = to_standard(curr_rna_asso_name)
    return map_used_asso_name_to_rna_asso_name



