# coding: utf-8
import copy
import datetime
import hashlib
import json
import os
import re

import fr_core_news_md
from text_toolkit.text_is_usefull import text_is_usefull

from loaders.load_topics import load_user_defined_topics
from utils.path_generation import get_output_enriched_docs_path, get_input_path
from utils.utils import SetEncoder, get_list_from_file
from utils.utils_for_text import to_standard, standardized_list
from utils.utils_ner_results import clean_location_entity_name

list_loc_of_interest = get_list_from_file(get_input_path('list_locations_of_interest.txt'))

nlp = fr_core_news_md.load()


def doc_topic_is_sport(doc):
    """
    Test if the doc has a sport theme
    :param doc:
    :return:
    """
    if "sujet" in doc.keys() and "gspo" in doc["sujet"]:
        return True
    return False


def get_doc_date(doc_date):
    if isinstance(doc_date, int):
        return (datetime.datetime.fromtimestamp(doc_date / 1e3)).strftime("%Y-%m-%d")
    return doc_date


def get_doc_file_name(doc):
    hash_key = int(hashlib.sha1(doc['corps'].encode()).hexdigest(), 16) % (10 ** 8)
    year, month, day = str(doc['date']).split('-')
    date = year + month + day
    return '%s_%s_%s.json' % (str(doc["source_code"]), date, str(hash_key))


def get_doc_path(doc, path):
    year, month, day = doc['date'].split('-')

    if not path:
        return os.path.join(get_output_enriched_docs_path(doc["source_code"]), year, month, get_doc_file_name(doc))
    else:
        return os.path.join(path, doc["source_code"], year, month, get_doc_file_name(doc))


def get_entities_from_doc(text, ner_class, params_ner, overwrite=True):
    params_ner['text'] = text
    return ner_class(**params_ner)


def get_doc_topics(doc: dict):
    """
    topics are specified in a separated file, ./input/load_topics.json
    :param doc:
    :return:
    """
    doc['themes'] = []
    text = doc['corps']
    topics = load_user_defined_topics()
    for topic in topics:
        new_body = text
        for regex in topics[topic]['to_exclude']:
            new_body = re.sub(regex.lower(), '', (new_body.lower()))
        for word in topics[topic]['to_include']:
            if (to_standard(word, keep_spaces=True) in to_standard(new_body, keep_spaces=True)) or (
                    re.search(word, new_body)):
                doc['themes'].append(topic)
                break
    return doc


def get_doc_locations(doc, only_in_loc_of_interest=True):
    """
    return the doc with a new field 'location' containing the location
    :param doc:
    :param only_in_loc_of_interest: if  true, only the locations that are among in the locations of interest are kept
    :return:
    """
    doc_ner_res = copy.deepcopy(doc['ner_results'])
    map_std_name_to_real_location_name = {to_standard(location_name): location_name for location_name in
                                          list_loc_of_interest}
    if only_in_loc_of_interest:
        std_list_loc_of_interest = standardized_list(list_loc_of_interest)
        doc['locations'] = list(set([clean_location_entity_name(curr_ner_res, map_std_name_to_real_location_name.get(
            to_standard(curr_ner_res['text']), curr_ner_res['text']))['text'] for curr_ner_res in doc_ner_res if
                                     curr_ner_res['label_'] == 'LOC' and to_standard(
                                         curr_ner_res['text']) in std_list_loc_of_interest]))
    else:
        doc['locations'] = list(set([clean_location_entity_name(curr_ner_res, map_std_name_to_real_location_name.get(
            to_standard(curr_ner_res['text']), curr_ner_res['text']))['text'] for curr_ner_res in doc_ner_res if
                                     curr_ner_res['label_'] == 'LOC']))
    return doc


def save_doc(doc, path):
    path_doc = get_doc_path(doc, path)
    if not os.path.isdir(os.path.dirname(os.path.dirname(os.path.dirname(path_doc)))):
        os.makedirs(os.path.dirname(os.path.dirname(os.path.dirname(path_doc))))
    if not os.path.isdir(os.path.dirname(os.path.dirname(path_doc))):
        os.makedirs(os.path.dirname(os.path.dirname(path_doc)))
    if not os.path.isdir(os.path.dirname(path_doc)):
        os.makedirs(os.path.dirname(path_doc))
    js = json.dumps(doc, cls=SetEncoder)
    fp = open(path_doc, 'w')
    fp.write(js)


def build_doc(doc, source_normalizer):
    return source_normalizer.map_doc_original_key_to_lmp_key(doc)


def enrich_doc(doc, entities=True, loc=True, topic=True, loc_of_interest=False):
    """
    Enrich doc with some new informations
    :param doc:
    :param entities: if True, add entities form NER
    :param loc: if True, add locations
    :param topic: if True, add topics
    :param loc_of_interest: add locations only in loc of interest
    :return:
    """
    try:
        if not (doc_topic_is_sport(doc)) and doc['corps'] and text_is_usefull(doc['corps']):
            global nlp
            if entities:
                doc["ner_results"] = [
                    {'text': ent.text, 'label_': ent.label_, 'start_char': ent.start_char, 'end_char': ent.end_char} for
                    ent in
                    nlp(doc['corps']).ents]
            if topic:
                doc = get_doc_topics(doc)
            if loc:
                doc = get_doc_locations(doc, only_in_loc_of_interest=loc_of_interest)
            return doc
    except KeyError:
        print("oups, the fields are not all their, there is only %s" % doc.keys())
