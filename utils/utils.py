# coding: utf-8

import os
import json
import datetime
import time
from colorama import Fore, Style
from utils.utils_for_text import to_standard

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        if isinstance(obj, time):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)


def merge_list_of_dictionaries(dict):
    results = {}
    for d in dict:
        results.update(d)
    return results


def get_list_from_file(filepath):
    f = open(filepath, 'r')
    list_of_words = [line.rstrip(os.linesep) for line in f]
    f.close()
    return list_of_words


def print_entities_for_selection(list_entities, corpus):
    print(' '.join([ent.name for ent in list_entities]))
    for ent in list_entities:
        print(corpus[ent.doc]['corps'][
              max(0, ent.start_char - 600): ent.start_char])
        print(Fore.RED + corpus[ent.doc]['corps'][ent.start_char:ent.end_char], end="")
        print(Style.RESET_ALL, end="")
        print(corpus[ent.doc]['corps'][ent.end_char: min(ent.end_char + 600, len(corpus[ent.doc]['corps']) - 1)],
              end="")
        print('\n')


def mandate_in_documents(list_entities, corpus):
    """

    :param list_entities: list of Entity objects with the same Entity.id
    :param corpus: dictionary of documents.
    :return: true if in one of the entities in list_entities
    """
    for ent in list_entities:
        for curr_mandat in ['maire', 'prefet', 'senateur', 'depute', 'ministre']:
            if (curr_mandat in to_standard(corpus[ent.doc]['corps'][max(0, ent.start_char - 20): ent.start_char])) or (curr_mandat in to_standard(corpus[ent.doc]['corps'][ent.end_char: min(ent.end_char + 20, len(corpus[ent.doc]['corps']) - 1)])):
                return True
    return False


def is_author(list_entities, doc_dict, threshold=0.7):
    """
    a named entity is the author of the document if it's always quoted at the begining or at the end of the doc.
    :return: true if the named entity is the author of the doc
    """
    occurrence_of_ent_name_at_end_or_beg_of_doc = sum(
        [1 for ent in list_entities if ent.at_end_or_beg_of_doc(doc_dict[ent.doc]['corps'])])
    return (occurrence_of_ent_name_at_end_or_beg_of_doc / len(list_entities) >= threshold)


def format_dict_entities_for_reloading(dict_entities):
    to_save = {}
    for ent in dict_entities:
        to_save[ent] = {}
        to_save[ent]['entities'] = [curr_ent.format_entity_for_json() for curr_ent in
                                    dict_entities[ent]['entities']]
        to_save[ent]['pertinence'] = dict_entities[ent]['pertinence']
        to_save[ent]['importance'] = dict_entities[ent]['importance']
    return to_save


def get_hastable_from_obj_dict(obj_dict):
    optimised_obj_set = {}
    for id_obj in obj_dict:
        obj = obj_dict[id_obj]
        std_name = obj.get_std_name()
        if std_name in optimised_obj_set:
            optimised_obj_set[std_name].append(obj)
        else:
            optimised_obj_set[std_name] = [obj]
    return optimised_obj_set


