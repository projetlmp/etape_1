import json
from jsonschema import validate
from input.output_format_keys import dict_ent_attribute_keys, dict_contexte_attribute_keys
from loaders.load_topics import load_user_defined_topics
from utils.path_generation import get_input_path
from config.path import path_to_schema
from text_toolkit.load_list_from_file import load_list_from_file

def verify_attributes_key_before_saving(attribute_to_check, dict_attribute_to_check_keys):
    for key in dict_attribute_to_check_keys:
        if key in attribute_to_check:
            for val in attribute_to_check[key]:
                assert set(list(val.keys())).difference(set(dict_attribute_to_check_keys[key])) == set()
    return True


def verify_output_values_before_saving(to_save):
    with open(path_to_schema) as f:
        schema = json.load(f)
    assert (to_save != [])
    locations = load_list_from_file(get_input_path('list_locations_of_interest.txt'))
    print(locations)
    topics = load_user_defined_topics()
    print(topics)
    influenceurs = [to_save[i]['name'] for i in range(len(to_save))]
    assert all([verify_attributes_key_before_saving(ent, dict_ent_attribute_keys) for ent in to_save])
    assert all(
        [verify_attributes_key_before_saving(curr_contexte, dict_contexte_attribute_keys) for ent in to_save for
         curr_contexte in ent['contextes']])
    assert all([set(list(ent.keys())).difference(
        {'locations', 'name_loc_asso', 'is_association', 'total_mentions', 'is_other', 'themes',
         'association_description', 'name', 'id', 'mandates', 'contextes', 'is_elu', 'pertinence',
         'date_creat_asso'}) == set() for ent in to_save])
    assert all([ent['total_mentions'] >= ent['pertinence'] for ent in to_save])
    assert all([len(ent['contextes']) == ent['total_mentions'] for ent in to_save])
    assert all([set([loc['name'] for loc in ent['locations']]).difference(set(locations)) == set() for ent in to_save])
    assert all([set([theme['name'] for theme in ent['themes']]).difference(set(topics)) == set() for ent in to_save])
    assert all([ent['mandates'] for ent in to_save if ent['is_elu']])
    assert all([isinstance(curr_theme['name'], str) for ent in to_save for curr_theme in ent['themes']])
    assert all([isinstance(curr_loc['name'], str) for ent in to_save for curr_loc in ent['locations']])
    assert all([isinstance(curr_loc['name'], str) for ent in to_save for curr_contexte in ent['contextes'] for curr_loc in curr_contexte['locations']])
    assert all([curr_influenceur['name'] in influenceurs for ent in to_save for curr_contexte in ent['contextes'] for curr_influenceur in curr_contexte['influenceurs']])
    validate(to_save, schema=schema)
    return True

