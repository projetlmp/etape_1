import json
import os
from functools import partial
from json import JSONDecodeError
from multiprocessing import Pool

import fr_core_news_md
import numpy as np
from fastavro import reader
from text_toolkit.text_is_usefull import text_is_usefull
from text_toolkit.text_normalisation import process_text_for_nlp

from loaders.corpus_normalizer import CorpusSourceNormalizer
from utils.path_generation import get_doc_path
from utils.utils_for_doc import doc_topic_is_sport, get_doc_topics, save_doc, get_doc_locations, build_doc, enrich_doc
from utils.utils_for_text import text_embedding, get_adjacency_matrix
from utils.utils_ner_results import get_dict_entities_from_doc, merge_dict_entities

nlp = fr_core_news_md.load()


def build_idf(corpus):
    """

    :param corpus: a dictionnary of texts corpus = {id_1: text_1, ..., id_n: text_n}, where text_i is a string.
    :return: {idf[w_1] = idf(w_1), ...,idf(w_#vocabulary) = idf(w_#vocabulary)} for vocabulary = set(word for word in
    corpus).
    """
    idf = {}
    len_processed_articles = len(corpus)
    for id_doc in corpus:
        for w in set(corpus[id_doc]):
            idf[w] = idf.get(w, 0) + 1
    for w in idf:
        idf[w] = max(1, np.log10(len_processed_articles / (idf[w])))
    return idf


def add_entities_to_corpus(corpus, package):
    """
    Add the ner results to the corpus. The ner can be generated using different packages
    :param corpus: dictionnary
    :param package: name of the package to use. Currently only "spacy"
    :return:
        params_ner = {}
    if package == "google nlp":
        try:
            from loaders.entities.entities_google import EntityGoogle, GoogleNer, client_instantiation
            ner_class = GoogleNer
            params_ner['google_client'] = client_instantiation()
        except ImportError:
            import fr_core_news_md
            nlp = fr_core_news_md.load()
    else:
        import fr_core_news_md
        nlp = fr_core_news_md.load()
    for doc in iter_doc(corpus):
        ner_res = nlp(doc['corps']).ents
        corpus[doc['id']]['ner_results'] = [
            {'text': ent.text, 'label_': ent.label_, 'start_char': ent.start_char, 'end_char': ent.end_char} for ent in
            ner_res]
    """
    if package == "spacy":
        import fr_core_news_md
        nlp = fr_core_news_md.load()
        for doc in iter_doc(corpus):
            corpus[doc["id"]]["ner_results"] = [
                {'text': ent.text, 'label_': ent.label_, 'start_char': ent.start_char, 'end_char': ent.end_char} for ent
                in nlp(doc['corps']).ents]
    return corpus


def add_themes_to_corpus(corpus):
    """
    Add theme for all the elemnents of the corpus
    :param corpus:
    :return:
    """
    for doc in iter_doc(corpus):
        corpus[doc['id']] = get_doc_topics(doc)
    return corpus


def add_locations_to_corpus(corpus):
    """
    Add a location to all the elements in the corpus
    :param corpus:
    :return: corpus with a new
    """
    for doc in iter_doc(corpus):
        corpus[doc['id']] = get_doc_locations(doc)
    return corpus


def add_influenceurs(corpus, entities_set):
    for ent in entities_set.dict_entities:
        ent_docs = set([curr_ent.doc for curr_ent in entities_set.dict_entities[ent]['entities']])
        for doc in ent_docs:
            try:
                corpus[doc]['influenceurs'].append(
                    {"name": next(iter(entities_set.dict_entities[ent]['entities'])).name})
            except KeyError:
                corpus[doc]['influenceurs'] = [
                    {"name": next(iter(entities_set.dict_entities[ent]['entities'])).name}]
    return corpus


def remove_corpus_without_influenceurs(corpus):
    return {id_doc: corpus[id_doc] for id_doc in corpus if 'influenceurs' in corpus[id_doc]}


def read_corpus(corpus_path, encoding='latin1'):
    file_path = get_doc_path(corpus_path)
    for file in file_path:
        try:
            if os.path.splitext(file)[1] == '.avro':
                with open(file, 'rb') as fo:
                    a = reader(fo)
                    for article in a:
                        try:
                            yield article
                        except TypeError:
                            print("Type error, body is probably empty")
            else:
                with open(os.path.join(file), 'r', encoding=encoding) as f:
                    a = json.load(f)
                try:
                    if 'source_code' not in a:
                        a['source_code'] = file.split('/')[-1].split('_')[0]
                    yield a
                except TypeError:
                    print("Type error, body is probably empty")
        except AssertionError:
            print("Assertion error reading the file at %s" % corpus_path)
        except JSONDecodeError as e:
            print("Problem %s decoding file %s" % (repr(e), file_path))


def iter_doc(corpus):
    for doc_id in corpus.keys():
        try:
            yield corpus[doc_id]
        except TypeError:
            print("problem in the file for doc_id %s" % doc_id)


def iter_doc_corps(corpus):
    for doc_id in corpus.keys():
        yield corpus[doc_id]['corps']


def remove_useless_doc(corpus):
    """
    Remove sport docs and docs where the text is not usefull
    :param corpus:
    :return:
    """
    corpus_without_useless_doc = {}
    for doc in iter_doc(corpus):
        try:
            if not doc_topic_is_sport(doc) and text_is_usefull(doc['corps']):
                corpus_without_useless_doc[doc['id']] = doc
        except BaseException as e:
            print("Error %s with article %s" % (repr(e), doc['titre']))
    return corpus_without_useless_doc


def process_corpus(corpus):
    return {id_doc: process_text_for_nlp(corpus[id_doc]['corps']) for id_doc in corpus}


def find_non_redudant_docs(score_adjacency_matrix, threshold=0.99):
    non_redudant_docs = list(score_adjacency_matrix.keys())
    for doc_1 in score_adjacency_matrix:
        for doc_2 in score_adjacency_matrix[doc_1]:
            if score_adjacency_matrix[doc_1][doc_2] > threshold:
                try:
                    non_redudant_docs.remove(doc_2)
                except ValueError:
                    pass
    return non_redudant_docs


def remove_redundant_docs_optimised(dict_entities, corpus, threshold):
    score_adjacency_matrix = {}
    for id_entity in dict_entities:
        docs = [ent.doc for ent in dict_entities[id_entity]['entities']]
        docs_to_del = []
        for i in range(len(docs)):
            for j in range(i + 1, len(docs)):
                try:
                    val = score_adjacency_matrix[docs[i]][docs[j]]
                except KeyError:
                    val = ''
                    score_adjacency_matrix[docs[i]][docs[j]] = val
                    score_adjacency_matrix[docs[j]][docs[i]] = val
                if val > threshold:
                    docs_to_del.append(j)
        dict_entities[id_entity]['entities'] = [dict_entities[id_entity]['entities'][i] for i in
                                                range(len(dict_entities[id_entity]['entities'])) if
                                                i not in docs_to_del]


def remove_redundant_docs(corpus):
    processed_corpus = process_corpus(corpus)
    idf = build_idf(processed_corpus)
    dict_doc_embedding = {id_doc: text_embedding(idf, processed_corpus[id_doc]) for id_doc in processed_corpus}
    score_adjacency_matrix = get_adjacency_matrix(dict_doc_embedding)
    non_redudant_docs = find_non_redudant_docs(score_adjacency_matrix)
    return {k: corpus[k] for k in non_redudant_docs}


def build_corpus(corpus_source,
                 corpus_path,
                 enriched_corpus_saving_path=None,
                 parallelize=False,
                 only_in_loc_of_interest=False,
                 n_proc=1,
                 chunksize=10):
    """
    Build a list of formatted and enriched documents with their named entities, their locations and their topics.
    :param corpus_source: one of ["EDD", "Scrap", "DJ"]
    :param corpus_path: path where the raw corpus is stored
    :param enriched_corpus_saving_path: where to save the enriched_corpus
    :param parallelize: true if want to parallelize the function.
    :param only_in_loc_of_interest: add only locations that are in "loc_of_interest"
    :param n_proc: if parallelize, number of cores on which to parallelize the function. Has to be at most #cpus - 1.
    :param chunksize: if parallelize, size of the batches on wich to do parallelized calls.
    :return: a list of enriched documents.
    """
    source_normalizer = CorpusSourceNormalizer(corpus_source)
    if parallelize:
        p = Pool(n_proc)
        partial_build_doc = partial(build_doc, source_normalizer=source_normalizer)
        partial_enriched_doc = partial(enrich_doc, loc_of_interest=only_in_loc_of_interest)
        built_corpus = p.imap_unordered(partial_build_doc, read_corpus(corpus_path), chunksize)
        enriched_corpus = list(p.imap_unordered(partial_enriched_doc, built_corpus, chunksize))
        p.close()
        p.join()
    else:
        built_corpus = [build_doc(doc, source_normalizer) for doc in read_corpus(corpus_path)]
        enriched_corpus = [enrich_doc(doc) for doc in built_corpus]
    source_normalizer.handle_unknown_sources()
    corpus_to_save = {doc["id"]: doc for doc in enriched_corpus if doc}
    save_corpus(corpus_to_save, path=enriched_corpus_saving_path)
    return [doc for doc in enriched_corpus if doc]


def get_entities_from_list_of_doc(corpus: list, package: str, parallelize=False, n_proc=1) -> dict:
    """
    This function extract entities from a corpus
    :param corpus: to extract entities from
    :param package: package to use to extract entities
    :param parallelize: dow we parallelize the process ?
    :param n_proc: how many proc do we use
    :return: a dict that contains the entities
    """
    dict_entities = {}
    if parallelize:
        p = Pool(n_proc)
        partial_get_dict_entities_from_doc = partial(get_dict_entities_from_doc, package=package)
        for doc in p.imap_unordered(partial_get_dict_entities_from_doc, corpus):
            dict_entities = merge_dict_entities(doc, dict_entities)
        p.close()
        p.join()
    else:
        for doc in corpus:
            curr_dict_entities = get_dict_entities_from_doc(doc, package=package)
            dict_entities = merge_dict_entities(curr_dict_entities, dict_entities)
    return dict_entities


def save_corpus(corpus: dict, path=None):
    """
    Save the articles of the corpus as json files in an arborescence
    :param corpus: a dict that contains articles
    :param path: the root path where to save the articles
    :return: True if succeed
    """
    for doc_id in corpus:
        save_doc(corpus[doc_id], path)
    return True


def load_json_saved_corpus(saved_corpus_path: str) -> list:
    """
    Load a corpus as a list of docs from a path.
    :param saved_corpus_path: the path is the root where the articles are json files.
    :return:
    """
    corpus = []
    for r, d, f in os.walk(saved_corpus_path):
        for file in f:
            try:
                with open(os.path.join(r, file), 'r') as json_file_name:
                    if os.path.splitext(file)[1] == '.json':
                        corpus.append(json.load(json_file_name))
            except JSONDecodeError as e:
                print("Problem %s decoding file %s" % (repr(e), file))
            except UnicodeDecodeError as e:
                print("Problem %s decoding file %s" % (repr(e), file))
    return corpus
