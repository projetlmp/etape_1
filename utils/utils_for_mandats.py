from config.config_elus import dict_mandat_elu_fonction, dict_mandat_elu_surname
import math

def get_mandat_name_CM(mandat, data):
    return data[dict_mandat_elu_fonction[mandat]].apply(
        lambda x: x if (not isinstance(x, str)) and (not math.isnan(x)) else 'Membre du Conseil Municipal')


def get_mandat_name_EPCI(mandat, data):
    return data[dict_mandat_elu_fonction[mandat]].apply(
        lambda x: x if (not isinstance(x, str)) and (
            not math.isnan(x)) else "Membre de l'EPCI")


def get_mandat_name_std(mandat, data):
    dict_mandat_name_std = {"Maires.txt": u"Maire",
                            "groupements.csv": u"Président du groupement",
                            "Senateurs.txt": u"Sénateur",
                            "Deputes.txt": u'Député',
                            "CR.txt": u"Conseiller régional",
                            "CD.txt": u'Conseiller départemental'}
    return [dict_mandat_name_std[mandat]] * len(data)


mandat_action_map = {"CM 01 50.txt": get_mandat_name_CM,
                     "CM 51 OM.txt": get_mandat_name_CM,
                     "EPCI.txt": get_mandat_name_EPCI,
                     "Maires.txt": get_mandat_name_std,
                     "groupements.csv": get_mandat_name_std,
                     "Senateurs.txt": get_mandat_name_std,
                     "Deputes.txt": get_mandat_name_std,
                     "CR.txt": get_mandat_name_std,
                     "CD.txt": get_mandat_name_std
                     }
