import os
import six

from config.path import path_google_cloud_cred
from entities.entities_google import GoogleNer
from entities.entities_spacy import SpacyNer

# Imports the Google Cloud client library
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = path_google_cloud_cred

from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types

# Instantiates a client
google_entity_type = ('UNKNOWN', 'PERSON', 'LOCATION', 'ORGANIZATION',
                      'EVENT', 'WORK_OF_ART', 'CONSUMER_GOOD', 'OTHER')
mention_type = ('TYPE_UNKNOWN', 'PROPER', 'COMMON')


def client_instantiation():
    return language.LanguageServiceClient()


def get_google_entities_from_text(text, client):
    """Detects entities in the text."""

    if isinstance(text, six.binary_type):
        text = text.decode('latin1')

    # Instantiates a plain text document.
    document = types.Document(
        content=text,
        type=enums.Document.Type.PLAIN_TEXT)

    # Detects entities in the document. You can also analyze HTML with:
    #   document.type == enums.Document.Type.HTML
    entities = client.analyze_entities(document).entities

    # entity types from enums.Entity.Type

    return entities

def get_google_ner_class(params_ner):
    try:
        ner_class = GoogleNer
        params_ner['google_client'] = client_instantiation()
    except ImportError:
        ner_class = SpacyNer
    return ner_class, params_ner