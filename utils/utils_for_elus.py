import math
from utils.utils_for_mandats import mandat_action_map
from config.config_elus import dict_mandat_loc, dict_mandat_elu_id, dict_mandat_elu_name, dict_mandat_elu_surname, \
    dict_mandat_nuance_politique, \
    dict_mandat_elu_fonction, ordre_mandat


def get_elus_id(mandat, data):
    if mandat in dict_mandat_elu_id.keys():
        return data[dict_mandat_elu_id[mandat]]
    else:
        if mandat == "groupements.csv":
            return list(map(lambda x: 'PRES' + str(x[0]) + str(x[1]),
                            list(zip(data[dict_mandat_elu_name[mandat]], data[dict_mandat_elu_surname[mandat]]))))


def get_elus_nuance_politique(mandat, data):
    if mandat in dict_mandat_nuance_politique.keys():
        return data[dict_mandat_nuance_politique[mandat]].apply(
            lambda x: x if (isinstance(x, str)) else 'Pas de nuance politique')
    else:
        if mandat == "groupements.csv":
            return ['Pas de nuance politique'] * len(data[dict_mandat_elu_surname[mandat]])


def get_elus_mandat_name(mandat, data):
    try:
        return mandat_action_map[mandat](mandat, data)
    except KeyError:
        return data[dict_mandat_elu_fonction[mandat]].apply(
            lambda x: x if (not isinstance(x, str)) and (not math.isnan(x)) else '')


def get_elus_loc_list(mandat, data):
    dict_loc_lists = {}
    mandat_loc_config = dict_mandat_loc[mandat]
    n = len(data)
    for curr_loc_granularity in mandat_loc_config:
        try:
            curr_loc_granularity_column = mandat_loc_config[curr_loc_granularity][0]
            mapping_to_std_location_format = mandat_loc_config[curr_loc_granularity][1]
            dict_loc_lists[curr_loc_granularity] = list(map(mapping_to_std_location_format, data[curr_loc_granularity_column]))
        except TypeError:
            dict_loc_lists[curr_loc_granularity] = [-1]*n
    return dict_loc_lists


def save_elu(self, json_file, id_entity):
    global elus_set
    mandats = sorted(elus_set.dict_elus[self.dict_entities[id_entity].elu].mandats,
                     key=lambda x: ordre_mandat[x.fonction])
    json_file['mandates'] = []
    for i, mandat in enumerate(mandats):
        json_file['mandates'].append(mandat.__to_dict__())
    json_file['main_location'] = json_file['mandates'][0]['nom_loc']
    return json_file

