# coding: utf-8
from text_toolkit.text_normalisation import normalise


def to_standard(text, keep_spaces=False, map_used_asso_name_to_rna_asso_name=False):
    """

    :param text: string to standardize.
    :param keep_spaces: set to true to keep the spaces in the text, otherwise spaces will be deleted.
    :param map_used_asso_name_to_rna_asso_name:
    :return: input text lowercase, without accents, without punctuation.
    """
    text = normalise(text, keep_spaces)
    if map_used_asso_name_to_rna_asso_name:
        try:
            return map_used_asso_name_to_rna_asso_name[text]
        except KeyError:
            return text
    return text


def standardized_list(text_list, keep_spaces=False):
    return [to_standard(text, keep_spaces) for text in text_list]



def text_embedding(idf, text):
    from entities.word2vec import w2v
    text_embedding = 0
    for word in text:
        if word in w2v.word2vec:
            text_embedding = text_embedding + idf[word] * w2v.word2vec[word]
    return text_embedding


def get_adjacency_matrix(dict_doc_embedding):
    from entities.word2vec import w2v
    corpus_ids = list(dict_doc_embedding.keys())
    adjacency_matrix = {}
    for i in range(len(corpus_ids)):
        adjacency_matrix[corpus_ids[i]] = {}
        for j in range(i + 1, len(corpus_ids)):
            adjacency_matrix[corpus_ids[i]][corpus_ids[j]] = w2v.score(dict_doc_embedding[corpus_ids[i]],
                                                                       dict_doc_embedding[corpus_ids[j]])
    return adjacency_matrix
