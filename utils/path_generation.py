import os
from itertools import groupby
from operator import itemgetter

from config.path import path_output_enriched_docs, PROJECT_PATH, path_input_docs, data_elus_dir_name, \
    data_assos_dir_name, test_data_dir_name


def get_output_enriched_docs_path(filename):
    return os.path.join(path_output_enriched_docs, filename)


def get_input_data_path():
    return path_input_docs


def get_external_data_path(filename):
    return os.path.join(get_input_path('external_data'), filename)


def get_source_dic_path(filename="Sources_Titles_Keys.csv"):
    return os.path.join(PROJECT_PATH, "config/config_files", filename)


def get_input_path(filename):
    return os.path.join(PROJECT_PATH, 'input', filename)


def get_path_test_articles():
    return get_input_path('test_data')


def get_doc_path(corpus_path):
    file_path = []
    for path, subdirs, files in os.walk(corpus_path):
        for name in files:
            if name[0] != '.':
                file_path.append(os.path.join(path, name))
    return file_path


def get_data_elus_path():
    return get_external_data_path(data_elus_dir_name)


def get_data_asso_path():
    return get_external_data_path(data_assos_dir_name)


def get_path_test_docs():
    return get_input_path(test_data_dir_name)


def get_deepest_path(search_path):
    """
    Return a list of the deepest folders in a directory
    :param search_path:
    :return: list of path that contains files
    """
    all_files = []
    for root, dirs, files in os.walk(search_path):
        for file in files:
            relative_path = os.path.relpath(root, search_path)
            if relative_path == ".":
                relative_path = ""
            all_files.append(
                (relative_path.count(os.path.sep),
                 relative_path,
                 file
                 )
            )
    all_files.sort(reverse=True)
    list_deepest_paths = []
    for (count, folder), files in groupby(all_files, itemgetter(0, 1)):
        if len([filename for filename in list(files) if filename[2][0] != '.']) > 0:
            list_deepest_paths.append(os.path.join(search_path, folder))
    return list_deepest_paths
