from entities.word2vec import w2v
from utils.utils_for_corpus import process_corpus, build_idf, find_non_redudant_docs


def get_adjacency_matrix(dict_doc_embedding):
    corpus_ids = list(dict_doc_embedding.keys())
    adjacency_matrix = {}
    for i in range(len(corpus_ids)):
        adjacency_matrix[corpus_ids[i]] = {}
        for j in range(i + 1, len(corpus_ids)):
            adjacency_matrix[corpus_ids[i]][corpus_ids[j]] = w2v.score(dict_doc_embedding[corpus_ids[i]],
                                                                       dict_doc_embedding[corpus_ids[j]])
    return adjacency_matrix


def remove_redundant_docs(corpus):
    processed_corpus = process_corpus(corpus)
    idf = build_idf(processed_corpus)
    dict_doc_embedding = {id_doc: text_embedding(idf, processed_corpus[id_doc]) for id_doc in processed_corpus}
    score_adjacency_matrix = get_adjacency_matrix(dict_doc_embedding)
    non_redudant_docs = find_non_redudant_docs(score_adjacency_matrix)
    return {k: corpus[k] for k in non_redudant_docs}


def text_embedding(idf, text):
    text_embedding = 0
    for word in text:
        if word in w2v.word2vec:
            text_embedding = text_embedding + idf[word] * w2v.word2vec[word]
    return text_embedding
