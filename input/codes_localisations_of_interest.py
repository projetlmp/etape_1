from text_toolkit.load_list_from_file import load_list_from_file
from utils.path_generation import get_input_path

codes_localisation_of_interest = {"codes_departement": ["69"], "codes_insee_commune": load_list_from_file(get_input_path('codes_insee')), "codes_region": ["84"]}
