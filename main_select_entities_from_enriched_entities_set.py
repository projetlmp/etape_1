import pickle
from entities.entities import EntitiesSet
from loaders.load_data_elus import get_dict_elus
from loaders.load_data_assos import get_dict_assos
from utils.utils_for_corpus import (remove_redundant_docs, add_influenceurs,
                                    remove_corpus_without_influenceurs)

with open('./output/temp_doc_list.pickle', 'rb') as handle:
    tot_doc_list = pickle.load(handle)


entities_set = EntitiesSet(dict_entities={},
                           pre_loaded_path='temp/entities_after_enrichment.json')
doc_dict = {doc['id']: doc for doc in tot_doc_list}
print('Build Elus dictionary')
filtered_dict_elus = get_dict_elus(False, filter_by_loc=True)
print('Build Associations dictionary')
filtered_dict_assos = get_dict_assos(False, filter_by_loc=True)
print("Select entities")
entities_set.select_relevant_entities(corpus=doc_dict,
                                      dict_elus=filtered_dict_elus,
                                      dict_assos=filtered_dict_assos,
                                      nb_to_keep=500,
                                      manual_selection=True,
                                      sort_by_pertinence=True,
                                      keep_only_persons=True,
                                      keep_all_elu=True)
print('Add influencers to corpus')
corpus_with_influenceurs = add_influenceurs(doc_dict, entities_set)
print('Remove corpus without influenceurs')
corpus_with_influenceurs_only = remove_corpus_without_influenceurs(corpus_with_influenceurs)
entities_set_docs = set(
    [e.doc for ent in entities_set.dict_entities for e in entities_set.dict_entities[ent]['entities']])
small_corpus = {doc_id: corpus_with_influenceurs_only[doc_id] for doc_id in entities_set_docs}
print('Remove redundant docs')
small_corpus = remove_redundant_docs(small_corpus)
print('Add contextes')
entities_set.add_contextes(small_corpus)
entities_set.save_for_dev(small_corpus, filename='entities_eurovia_final.json', path=None)