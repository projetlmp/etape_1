import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setuptools.setup(
    name="etape_1",
    version="0.0.1",
    url="https://Marjolaine_Grunenberger@bitbucket.org/datalmp/etape_1.git",
    packages=setuptools.find_packages(),
    install_requires = requirements,
)
