import configparser
import os
import pickle

import click

from multiprocessing import Pool

from loaders.get_ner import merge_list_of_dict_entities, \
    generate_and_process_entities_set
from utils.path_generation import get_deepest_path
from utils.utils_for_corpus import load_json_saved_corpus, get_entities_from_list_of_doc
from utils.utils_for_doc import get_doc_topics, get_doc_locations

Config = configparser.ConfigParser()
Config.read('config/config.ini')
corpus_source = Config.get('Source', 'corpus_source')
package = Config.get('Package', 'package')


@click.command()
@click.option('--corpus_path', '-i', default='./input_data', help='Path that contains all the enriched articles')
@click.option('--nb_entities', '-ne', default=4000, help='Number of entities to keep')
@click.option('--manual', '-m', is_flag=True, help='Number of entities to keep')
@click.option('--load_from_temp', '-temp', is_flag=True, help='if true, load from temporary entities file')
@click.option('--sort_by_relevance', '-r', is_flag=True, help='if true, output is presented by relevance')
@click.option('--filter_by_loc', '-loc', is_flag=True, help='if true, filter entities by loc')
@click.option('--keep_only_persons', '-pers', is_flag=True, help='if true, only persons are kept')
@click.option('--keep_all_elu', '-elu', is_flag=True, help='if true, elu are automatically included')
@click.option('--output_filename', '-o', default='entities_final.json', help='name of output file')
@click.option('--update_topics', '-top', is_flag=True, help='update topics on loaded docs')
@click.option('--update_locations', '-loca', is_flag=True, help='update locations on loaded docs')
def get_entities_from_enriched_corpus(corpus_path,
                                      nb_entities,
                                      manual,
                                      load_from_temp,
                                      sort_by_relevance,
                                      filter_by_loc,
                                      keep_only_persons,
                                      keep_all_elu,
                                      output_filename,
                                      update_topics,
                                      update_locations,
                                      nb_doc_by_chunks=1000):
    """
    This function allows to extract entities directly from a corpus. It does it by successively processing
    the subdirectories in the corpus path, and merging the lists of entities afterwards.
    :param corpus_path:
    :param nb_entities:
    :param manual:
    :param load_from_temp: do we load the
    :param sort_by_relevance: in the manual selection, put first the most relevant entities (if false, the first are
    the most important)
    :param filter_by_loc: keep only entities that are mentionned in one of the cities listed in "list_of_locations.txt"
    :param keep_only_persons: keep only entities of type person
    :param keep_all_elu: keep all entities that have the property "elu = True"
    :param output_filename: filename of the output entities file
    :param update topics: update the topics of the document using the topics currently in 'input/topics.json'
    :param update locations: update the locations of the document using the locations in
    'input/list_locations_of_interest.json'
    :param nb_doc_by_chunks: number of docs to divide the document in when updating topic or locations
    :return:
    """
    list_of_dict_entities = []
    tot_doc_list = []
    if load_from_temp:
        print("Loading data from temp files")
        dict_entity = {}
        with open('./output/temp_doc_list.pickle', 'rb') as handle:
            tot_doc_list = pickle.load(handle)
        if update_topics:
            print("Update topics")
            p = Pool()
            tot_doc_list = list(p.imap_unordered(get_doc_topics, tot_doc_list, nb_doc_by_chunks))
        if update_locations:
            print("Update locations")
            p = Pool()
            tot_doc_list = list(p.imap_unordered(get_doc_locations, tot_doc_list, nb_doc_by_chunks))
    else:
        for documents_directory in get_deepest_path(corpus_path):
            print("Loading corpus from %s" % documents_directory)
            if os.path.isdir(documents_directory):
                doc_list = load_json_saved_corpus(documents_directory)
                if update_topics:
                    print("Update topics")
                    p = Pool()
                    doc_list = list(p.imap_unordered(get_doc_topics, doc_list, nb_doc_by_chunks))
                if update_locations:
                    print("Update locations")
                    p = Pool()
                    doc_list = list(p.imap_unordered(get_doc_locations, doc_list, nb_doc_by_chunks))
                tot_doc_list += doc_list
                if not load_from_temp:
                    print("Extracting entities from %s" % documents_directory)
                    list_of_dict_entities.append(get_entities_from_list_of_doc(corpus=doc_list,
                                                                               package='spacy',
                                                                               parallelize=True,
                                                                               n_proc=4))
                else:
                    print("entities loaded from temp, no need to extract")
        print("Merging extracted entities")
        dict_entity = merge_list_of_dict_entities(list_of_dict_entities)
    with open('./output/temp_doc_list.pickle', 'wb') as handle:
        pickle.dump(tot_doc_list, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print("Enriching and saving entities")
    config_manual_selection = {
        'keep_only_persons': keep_only_persons,
        'keep_all_elus': keep_all_elu,
        'filter_by_loc': filter_by_loc,
        'manual_selection': manual

    }
    generate_and_process_entities_set(entity_dict=dict_entity,
                                      doc_list=tot_doc_list,
                                      overwrite=True,
                                      nb_entities=nb_entities,
                                      filename=output_filename,
                                      save_temp=False,
                                      load_from_temp=load_from_temp,
                                      sort_by_relevance=sort_by_relevance,
                                      **config_manual_selection
                                      )


if __name__ == '__main__':
    get_entities_from_enriched_corpus()
