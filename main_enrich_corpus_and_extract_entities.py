import configparser

import click

from main_enrich_corpus import save_enriched_corpus
from main_extract_entities_from_enriched_corpus import get_entities_from_enriched_corpus

Config = configparser.ConfigParser()
Config.read('config/config.ini')
enriched_corpus_saving_path_ini = Config.get('Paths', 'path_output_enriched_docs')


@click.command()
@click.option('--input_path', '-i', default='./input_data', help='Path that contains all the articles to use')
@click.option('--corpus_source', '-p', default='DJ', help='Where do the articles come from ?')
@click.option('--enriched_corpus_saving_path', default=enriched_corpus_saving_path_ini,
              help="Where to save the enriched corpus")
@click.option('--package', '-p', default='spacy', help='Which package do we use to treat it ?')
@click.option('--nb_entities', '-ne', default=4000, help='Number of entities to keep')
@click.option('--manual', '-m', default=True, help='Number of entities to keep')
def get_entities_from_corpus(input_path, corpus_source, enriched_corpus_saving_path, package, nb_entities, manual):
    assert (corpus_source in ["EDD", "Scrap", "DJ"])
    assert (package in ['spacy'])
    save_enriched_corpus(corpus_path=input_path, output_path=enriched_corpus_saving_path, corpus_source=corpus_source)
    get_entities_from_enriched_corpus(corpus_path=corpus_source, nb_entities=nb_entities, manual=manual)


if __name__ == '__main__':
    get_entities_from_corpus()
