import configparser
import os

import click

from utils.path_generation import get_deepest_path
from utils.utils_for_corpus import build_corpus

Config = configparser.ConfigParser()
Config.read('config/config.ini')
corpus_source_ini = Config.get('Source', 'corpus_source')
package = Config.get('Package', 'package')


@click.command()
@click.option('--corpus_path', '-i', default='./input_data', help='Path that contains all the docuemnts to use')
@click.option('--output_path', '-o', default='./output_data', help='Path  to save the enriched documents')
@click.option('--only_loc_of_interest', '-loc', is_flag=True, help='Add locations only from locations of interest')
@click.option('--corpus_source', '-s', default=corpus_source_ini,
              help='What is the source of the corpus (DJ, EDD, scrap)')
def save_enriched_corpus(corpus_path, output_path, corpus_source, only_loc_of_interest):
    """

    Build, enrich and save a corpus of documents.
    Enriched corpus is the corpus with enriched documents. Documents with sport tags and useless documents are removed.
    Enriched documents are documents with locations, topics and entities.
    """
    total_docs = []
    for paths in [get_deepest_path(path) for path in corpus_path.split(',')]:
        for subdir_to_process in paths:
            if os.path.isdir(subdir_to_process):
                print('Processing %s' % subdir_to_process)
                total_docs += build_corpus(corpus_source, corpus_path=subdir_to_process,
                                           enriched_corpus_saving_path=output_path,
                                           only_in_loc_of_interest=only_loc_of_interest
                                           )
    print('%s documents treated' % len(total_docs))


if __name__ == '__main__':
    save_enriched_corpus()
