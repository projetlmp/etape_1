import os

from config.path import name_intermediate_entities_output_file
from entities.entities import EntitiesSet
from loaders.load_data_elus import get_dict_elus
from loaders.load_data_assos import get_dict_assos
from utils.utils_for_corpus import (remove_redundant_docs, add_influenceurs,
                                    remove_corpus_without_influenceurs)
from utils.utils_ner_results import merge_dict_entities


def merge_list_of_dict_entities(list_of_entities_dict):
    """
    Merge dicts of entities in one
    :param list_of_entities_dict:
    :return:
    """
    entity_dict_tot = {}
    for entity_dict in list_of_entities_dict:
        entity_dict_tot = merge_dict_entities(entity_dict_tot, entity_dict)
    return entity_dict_tot


def generate_and_process_entities_set(entity_dict, doc_list, overwrite, nb_entities, manual_selection, filename,
                                      save_temp=True, load_from_temp=False, path_temp='./temp/',
                                      path_entities=None,
                                      sort_by_relevance=True,
                                      filter_by_loc=True,
                                      keep_only_persons=True,
                                      keep_all_elus=True):
    """
    From an entity dict, enrich and save the results in a format that is exploitable by the dev
    :param entity_dict: the entity dict to generate the output from
    :param doc_list: list of documents from which the entities dict has been extracted.
    :param overwrite: do we recreate the Elu et Assoc files
    :param nb_entities: number of entities to keep in the output
    :param manual_selection: is the selection done manually ?
    :param filename: what is the filename of the entity set to save
    :param save_temp: save the Entities set to be able to perform a new selection of entities without having to
    reprocess all the articles.
    :param load_from_temp: load the entities from an entity set saved when save temp was true
    :param path_entities: path where to save the entities output. If None, use the path specified in config.ini
    :param sort_by_relevance: how do we present the file ?
    :param filter_by_loc: do we filter the entities by locations specified in "list_of_locations_of_interest.txt"
    :return: Nothing
    """
    if load_from_temp:
        temp_file_path = os.path.join(path_temp, name_intermediate_entities_output_file)
        entities_set = EntitiesSet(dict_entities={},
                                   pre_loaded_path=temp_file_path)
    else:
        entities_set = EntitiesSet(dict_entities=entity_dict)

    if save_temp:
        entities_set.save_entities_sets_for_reloading('temp', 'entities.json')
    doc_dict = {doc['id']: doc for doc in doc_list}
    print("All enrichement")
    entities_set.do_all_enrichments(doc_dict, filter_by_loc=filter_by_loc)
    entities_set.save_entities_sets_for_reloading('temp', 'entities_after_enrichment.json')
    print('Build Elus dictionary')
    filtered_dict_elus = get_dict_elus(overwrite, filter_by_loc=filter_by_loc)
    print('Build Associations dictionary')
    filtered_dict_assos = get_dict_assos(overwrite, filter_by_loc=filter_by_loc)
    print("Select entities")
    entities_set.select_relevant_entities(corpus=doc_dict,
                                          dict_elus=filtered_dict_elus,
                                          dict_assos=filtered_dict_assos,
                                          nb_to_keep=nb_entities,
                                          manual_selection=manual_selection,
                                          sort_by_pertinence=sort_by_relevance,
                                          keep_only_persons=keep_only_persons,
                                          keep_all_elu=keep_all_elus)
    corpus_with_influenceurs = add_influenceurs(doc_dict, entities_set)
    corpus_with_influenceurs_only = remove_corpus_without_influenceurs(corpus_with_influenceurs)
    entities_set_docs = set(
        [e.doc for ent in entities_set.dict_entities for e in entities_set.dict_entities[ent]['entities']])
    small_corpus = {doc_id: corpus_with_influenceurs_only[doc_id] for doc_id in entities_set_docs}
    small_corpus = remove_redundant_docs(small_corpus)
    entities_set.add_contextes(small_corpus)
    entities_set.save_for_dev(small_corpus, filename=filename, path=path_entities)
