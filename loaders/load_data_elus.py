import json
import os
import pandas as pd
from config.config_elus import (config_mandat, ordre_mandat, dict_mandat_elu_surname,
                                dict_mandat_elu_name, dict_mandat_nom_loc)
from entities.elu import Elu
from entities.mandat import Mandat
from utils.path_generation import get_data_elus_path
from utils.utils_for_elus import get_elus_mandat_name, get_elus_nuance_politique, get_elus_id, get_elus_loc_list
from input.codes_localisations_of_interest import codes_localisation_of_interest

codes_insee_commune = codes_localisation_of_interest["codes_insee_commune"]
codes_departement = codes_localisation_of_interest["codes_departement"]
codes_region = codes_localisation_of_interest["codes_region"]


def elus_per_mandate_generator(data, mandat, filter_by_loc):
    """

    :param data: from the RNE
    :param mandat: from config_mandat
    :return: elu of the given type of mandate if is in locations of interest.
    """
    n_elus = len(data)
    dict_loc_lists = get_elus_loc_list(mandat, data)
    list_fonctions = get_elus_mandat_name(mandat, data)
    list_nuances_politiques = get_elus_nuance_politique(mandat, data)
    list_mandats = [
        Mandat({loc: dict_loc_lists[loc][i] for loc in dict_loc_lists}, data[dict_mandat_nom_loc[mandat]][i], list_fonctions[i], list_nuances_politiques[i]) for i in
        range(n_elus)]
    list_ids = get_elus_id(mandat, data)
    for i in range(n_elus):
        elu = Elu(data[dict_mandat_elu_surname[mandat]][i], data[dict_mandat_elu_name[mandat]][i], list_mandats[i],
                  list_ids[i])
        if filter_by_loc:
            if elu.is_elu_in(codes_insee_commune, codes_departement, codes_region):
                yield elu
        else:
            yield elu


def get_dict_elus_per_mandate(data, mandate, filter_by_loc):
    """

    :param data:
    :param mandate:
    :return: dict_elus_for_mandate = {'std(name_elu_1)': {'id_elu_homonyme_1_with_std_name_1':Elu(prenom='jean', nom='pierre', mandat='maire de truc'), 'id_elu_homonyme_2_with_std_name_1':ELu(prenom='jean', nom='pierre', mandat='deputé de lol'), ...}, ...}
    for each obj Elu(), it's mandates are all the mandates corresponding to the same id_elu in the RNE.
    the root key is the std(name_elu) to have all the homonymes at the same place.
    """
    dict_elus_for_mandate = {}
    for elu in elus_per_mandate_generator(data, mandate, filter_by_loc):
        elu_std_name = elu.get_std_name()
        if elu_std_name in dict_elus_for_mandate:
            try:
                dict_elus_for_mandate[elu_std_name][elu.id].mandats += elu.mandats
            except KeyError:
                dict_elus_for_mandate[elu_std_name][elu.id] = elu
        else:
            dict_elus_for_mandate[elu_std_name] = {}
            dict_elus_for_mandate[elu_std_name][elu.id] = elu
    return dict_elus_for_mandate


def dict_elus_generator(mandats=config_mandat, filter_by_loc=True):
    for curr_mandat in mandats:
        data = load_data_elus(curr_mandat)
        curr_dict_elus = get_dict_elus_per_mandate(data, curr_mandat, filter_by_loc)
        yield curr_dict_elus


def merge_dict_elus(dict_elus_1, dict_elus_2):
    for std_elu_name in dict_elus_2:
        if std_elu_name in dict_elus_1:
            for curr_id_elu_to_merge in dict_elus_2[std_elu_name]:
                try:
                    dict_elus_1[std_elu_name][curr_id_elu_to_merge].mandats += dict_elus_2[std_elu_name][
                        curr_id_elu_to_merge].mandats
                except KeyError:
                    dict_elus_1[std_elu_name][curr_id_elu_to_merge] = dict_elus_2[std_elu_name][curr_id_elu_to_merge]
        else:
            dict_elus_1[std_elu_name] = dict_elus_2[std_elu_name]
    return dict_elus_1


def get_dict_elus(overwrite=False, input_file='FilteredElusSet.json', output_file="FilteredElusSet.json",
                  filter_by_loc=True):
    if overwrite or not (os.path.isfile(os.path.join(get_data_elus_path(), input_file))):
        dict_elus = {}
        for curr_dict_elus in dict_elus_generator(filter_by_loc=filter_by_loc):
            dict_elus = merge_dict_elus(dict_elus, curr_dict_elus)
        save_dict_elus(dict_elus, output_file)
    else:
        dict_elus = load_saved_dict_elus(input_file)
    return dict_elus


def load_data_elus(source):
    if source.split('.')[-1] == 'txt':
        return pd.read_csv(os.path.join(get_data_elus_path(), source), encoding="latin", sep="\t", skiprows=[0],
                           dtype=str)
    if source.split('.')[-1] == 'csv':
        return pd.read_csv(os.path.join(get_data_elus_path(), source), encoding='latin', sep=";", dtype=str)


def save_dict_elus(dict_elus, output_file='ElusSet.json'):
    json_file = {}
    for std_elu_name in dict_elus:
        json_file[std_elu_name] = {}
        for curr_id_elu in dict_elus[std_elu_name]:
            elu = dict_elus[std_elu_name][curr_id_elu]
            json_file[std_elu_name][curr_id_elu] = {}
            mandats = sorted(elu.mandats, key=lambda x: ordre_mandat[x.fonction])
            json_file[std_elu_name][curr_id_elu]['mandats'] = {'mandat_%s' % i: mandat.__to_dict__() for i, mandat in
                                                               enumerate(mandats)}
            json_file[std_elu_name][curr_id_elu]['nom'] = elu.nom
            json_file[std_elu_name][curr_id_elu]['prenom'] = elu.prenom
            json_file[std_elu_name][curr_id_elu]['id'] = curr_id_elu
    js = json.dumps(json_file)
    fp = open(os.path.join(get_data_elus_path(), output_file), 'w')
    fp.write(js)
    fp.close()


def dict_elus_from_json(data):
    """
    Return a structur Elus set from a json describing the Elus
    :param data:
    :return: ElusSet structure
    """
    dict_elus = {}
    for std_elu_name in data:
        dict_elus[std_elu_name] = {}
        for curr_id_elu in data[std_elu_name]:
            elu_mandat_list = data[std_elu_name][curr_id_elu]['mandats']
            mandats = [
                Mandat(dict_loc=elu_mandat_list[mandat_index]['dict_loc'], nom_loc=elu_mandat_list[mandat_index]['nom_loc'],
                       fonction=elu_mandat_list[mandat_index]['fonction'],
                       couleur_politique=elu_mandat_list[mandat_index]['couleur_politique']) for mandat_index in
                elu_mandat_list]
            dict_elus[std_elu_name][curr_id_elu] = Elu(data[std_elu_name][curr_id_elu]['nom'],
                                                       data[std_elu_name][curr_id_elu]['prenom'], mandats, curr_id_elu)
    return dict_elus


def load_saved_dict_elus(input_file='ElusSet.json'):
    """
    Load json Elus file. The path is generated using the paths in config/path.py
    :param input_file: name of the input file
    :return:
    """
    try:
        with open(os.path.join(get_data_elus_path(), input_file)) as f:
            data = json.load(f)
    except FileNotFoundError:
        return get_dict_elus(overwrite=True, input_file=input_file)
    return dict_elus_from_json(data)
