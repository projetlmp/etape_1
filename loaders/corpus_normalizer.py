import pandas as pd

from config.config_normalisation_articles import dic_to_dic_source_fields, dic_to_dic_subject_fields
from utils.path_generation import get_source_dic_path
from utils.utils_for_doc import get_doc_date, get_doc_file_name


class CorpusSourceNormalizer:
    """
    This object contains all the information necessary to normalize text coming from a given sources.
    """

    def __init__(self, corpus_source):
        self.corpus_source = corpus_source
        self.source_code_dictionnary = load_original_source_code_dict(corpus_source)
        self.original_fields_dictionnary = load_original_field_dict(source=corpus_source)
        self.inverse_source_fields_dictionnary = {self.original_fields_dictionnary[k]: k for k in
                                                  self.original_fields_dictionnary}
        self.unknown_sources = set()
        self.subject_code_dictionnary = load_original_subject_dict(source=corpus_source)

    def map_subject_code_origin_to_lmp(self, subjects):
        """
        Transpose the subjects specific to one source (for instance gspo in DJ) to a common reference
        :param subjects: string with list of subjects separated by string
        :return: string with list of subjects in the common reference space, separated by string
        """
        new_subjects = []
        try:
            for s in subjects.split(','):
                try:
                    new_subjects.append(self.subject_code_dictionnary[s])
                except KeyError:
                    new_subjects.append(s)
            return (',').join(new_subjects)
        except AttributeError:
            return ''

    def map_source_origin_to_lmp(self, source_name):
        """
        For a given source code name, return a normalized 'lmp' name
        :param source_name: the source code in the original corpus
        :return: common source name
        """
        try:
            return self.source_code_dictionnary[source_name]
        except KeyError:
            self.unknown_sources.update([source_name])
            return source_name

    def map_lmp_to_original_field(self, field):
        """
        for a given field in the original dataset, return the normalized field name (for instance 'corps' => 'body',
        'titre' => 'title'...). The mapping is define in a configuration file, for each corpus source.
        :param field: field name in the original dataset
        :return: common field name
        """
        return self.original_fields_dictionnary[field]

    def handle_unknown_sources(self):
        """
        Store the sources that has not been find in the source dictionnary, in order to add it later.
        :return: nothing
        """
        source_colname = '%s Source Code' % self.corpus_source
        source_titles_keys = pd.read_csv(get_source_dic_path(filename="Sources_Titles_Keys.csv"), sep=';')
        for new_source in self.unknown_sources:
            try:
                index = list(source_titles_keys['LMP Source Code'].values).index(new_source)
                source_titles_keys[source_colname][index] = new_source
            except ValueError:
                source_titles_keys = source_titles_keys.append(
                    {'LMP Source Code': new_source, source_colname: new_source}, ignore_index=True)
        source_titles_keys.to_csv(get_source_dic_path(filename="Sources_Titles_Keys.csv"), sep=';', index=False)

    def map_doc_original_key_to_lmp_key(self, doc):
        """
        Transform the doc into the new reference.
        :param doc: the doc to transform to lmp doc
        :return: the doc transformed containing the new fields
        """
        doc_with_lmp_keys = {}
        dict_source_to_lmp_keys = self.inverse_source_fields_dictionnary
        for original_key in doc:
            if original_key in dict_source_to_lmp_keys.keys():
                lmp_key = dict_source_to_lmp_keys[original_key]
                if lmp_key == "sujet":
                    doc_with_lmp_keys[lmp_key] = self.map_subject_code_origin_to_lmp(doc[original_key])
                if lmp_key == "source_code":
                    doc_with_lmp_keys[lmp_key] = self.map_source_origin_to_lmp(doc[original_key])
                if lmp_key == "date":
                    doc_with_lmp_keys[lmp_key] = get_doc_date(doc[original_key])
                else:
                    doc_with_lmp_keys[lmp_key] = doc[original_key]
        if 'id' not in doc_with_lmp_keys and 'corps' in doc_with_lmp_keys:
            doc_with_lmp_keys['id'] = get_doc_file_name(doc_with_lmp_keys)
        return doc_with_lmp_keys


def load_original_source_code_dict(source='DJ'):
    """
    Load the dictionnay from csv file
    :param source: in ['DJ', 'EDD', 'Scrap']
    :return: a dict mapping Source code to LMP Source code
    """
    source_code_df = pd.read_csv(get_source_dic_path(filename="Sources_Titles_Keys.csv"), sep=';')
    return source_code_df.set_index(source + ' Source Code')['LMP Source Code'].to_dict()


def load_original_field_dict(source='DJ'):
    """
    Return the dictionnay of the field in the souce specified
    :param source: in ['DJ', 'EDD', 'Scrap']
    :return: a dict mapping Source code to LMP Source code
    """
    return dic_to_dic_source_fields.get(source)


def load_original_subject_dict(source="DJ"):
    """
    Return the dictionnay of subject in the source specified
    :param source:
    :return: a dict mapping Source code to LMP Source code
    """
    return dic_to_dic_subject_fields.get(source)
