import json
import os
import pandas as pd
from config.config_assos import rna_name
from entities.asso import Asso
from utils.path_generation import get_data_asso_path


def assos_generator(data, filter_by_loc):
    n_assos = len(data['id'])
    for i in range(n_assos):
        asso = Asso(data['titre'][i], data['adrs_codeinsee'][i], data['id'][i], data['objet'][i],
                    data['adrs_libcommune'][i], data['date_creat'][i])
        if filter_by_loc:
            if asso.is_asso_in():
                yield asso
        else:
            yield asso


def get_dict_assos_from_data(data, filter_by_loc):
    dict_asso = {}
    for asso in assos_generator(data, filter_by_loc):
        asso_std_name = asso.get_std_name()
        if asso_std_name in dict_asso:
            dict_asso[asso_std_name][asso.id] = asso
        else:
            dict_asso[asso_std_name] = {}
            dict_asso[asso_std_name][asso.id] = asso
    return dict_asso


def load_data_assos():
    return pd.read_csv(os.path.join(get_data_asso_path(), rna_name), encoding='latin', sep=";", dtype=str)


def save_dict_assos(dict_assos, output_file):
    json_file = {}
    for curr_std_asso_name in dict_assos:
        json_file[curr_std_asso_name] = {}
        for curr_id_asso in dict_assos[curr_std_asso_name]:
            asso = dict_assos[curr_std_asso_name][curr_id_asso]
            json_file[curr_std_asso_name][curr_id_asso] = {attr: value for attr, value in asso.__dict__.items()}
    js = json.dumps(json_file)
    fp = open(os.path.join(get_data_asso_path(), output_file), 'w')
    fp.write(js)
    fp.close()


def assos_dict_from_json(data):
    dict_assos = {}
    for std_asso_name in data:
        dict_assos[std_asso_name] = {}
        for curr_id_asso in data[std_asso_name]:
            asso = Asso(nom=data[std_asso_name][curr_id_asso]['nom'],
                        id_loc=data[std_asso_name][curr_id_asso]['id_loc'], id=data[std_asso_name][curr_id_asso]['id'],
                        objet=data[std_asso_name][curr_id_asso]['objet'],
                        name_loc=data[std_asso_name][curr_id_asso]['name_loc']
                        , date_creat=data[std_asso_name][curr_id_asso]['date_creat'])
            dict_assos[std_asso_name][curr_id_asso] = asso
    return dict_assos


def load_saved_dict_assos(input_file='FilteredAssosSet.json'):
    try:
        with open(os.path.join(get_data_asso_path(), input_file)) as f:
            data = json.load(f)
    except FileNotFoundError:
        return get_dict_assos(overwrite=True, input_file=input_file)
    return assos_dict_from_json(data)


def get_dict_assos(overwrite=False, input_file='FilteredAssosSet.json', output_file="FilteredAssosSet.json",
                   filter_by_loc=True):
    if overwrite or not (os.path.isfile(os.path.join(get_data_asso_path(), input_file))):
        data = load_data_assos()
        dict_assos = get_dict_assos_from_data(data, filter_by_loc)
        save_dict_assos(dict_assos, output_file)
    else:
        dict_assos = load_saved_dict_assos(input_file)
    return dict_assos
