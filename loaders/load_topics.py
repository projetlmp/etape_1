import json

from utils.path_generation import get_input_path


def load_user_defined_topics():
    with open(get_input_path('topics.json'), 'r') as f:
        topic_dict = json.load(f)
    return topic_dict
