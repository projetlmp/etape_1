# impacters-goodwill

Entrée : un corpus de documents textuels.   
Sortie : une impacter-list des impacters cités dans les documents détectés grâce à un algorithme de reconnaissance d'entités nommées.

### Définition de l'entrée :    

exemple : 

input :   
{"id": "1234",
"titre": "cc", 
"date": 1295827200000, 
"corps": "article de presse",
"source_code": "PARISIEN"
}

Les champs nécessaires dans le corpus en entrée sont : ['id', 'titre', 'corps', 'date', 'source_code'].    
- le champ id contient l'identifiant unique du document ;
- le champ titre contient son titre ;
- Le champ corps contient son texte ;  
- Le champ date est la date de publication ;  
- Le champ source_code est la source du document, par exemple Le Parisien.  

### Tester le format de l'entree
Avant de lancer le code, il faut s'assurer que les données ont les champs nécessaires.            
Pour le vérifier, il faut utiliser le code suivant :      
```
import os
import json
def get_doc_path(corpus_path):
    file_path = []
    for path, subdirs, files in os.walk(corpus_path):
        for name in files:
            if name[0] != '.':
                file_path.append(os.path.join(path, name))
    return file_path
def assert_corpus_has_requiered_keys(corpus_path):
    for file in file_path:
        with open(os.path.join(file), 'r', encoding='latin1') as f:
            doc = json.load(f)
            for key in map_lmp_filed_names_to_source_field_names:
                assert(map_lmp_filed_names_to_source_field_names[key] in list(doc.keys()))
```
exemple d'utilisation :   

```
map_lmp_filed_names_to_source_field_names = {
    "corps": "corps_from_source",
    "id": "id_from_source",
    "titre": "titre_from_source",
    "date": "date_from_source",
    'source_code': "source_code_from_source",
}
test_doc = {"corps_from_source": "je suis le corps de l'article", 
           "id_from_source": "0001",
           "titre_from_source": "je suis un titre",
           "date_from_source": "11-12-2018",
           "source_code_from_source": "20MINUTES"}
if not os.path.isdir('Test_format_docs'):
    os.makedirs('Test_format_docs')
js = json.dumps(test_doc)
fp = open('Test_format_docs/test_doc', 'w')
fp.write(js)
with open('Test_format_docs/test_doc', 'r', encoding="latin1") as f:
    test_doc = json.load(f)
corpus_path = 'Test_format_docs'
assert_corpus_has_requiered_keys(corpus_path)
```
Ici, on suppose que les données sont des jsons. Cependant, il est facile de modifier ce test pour lire d'autres types de
données.


### Définition de la sortie :   
Ce code retourne une impacters-list.   
Définitions :  
- impacters-list : liste de noms de personnes ou association et d'informations les concernant sauvegardée au format json ; 
- entité nommée : dans le cadre de ce projet, il s'agit d'une personne ou d'une association détectée par l'algorithme de
reconnaissance d'entités nommées ; 
- docs(ent) : l'ensemble des documents dans lesquels l'entité nommée ent apparaît ;
- themes(doc) : l'ensemble des thèmes qui taguent le document doc ;
- locations(doc) : l'ensemble des lieux d'intérêt pour le client cités dans le document doc.   
Chaque élément de la liste a les clés suivantes :
- 'id' : l'identifiant de l'entité nommée ;    
- 'is_association' : vrai si l'entité nommée est une association ;
- 'check_if_elu': vrai si l'entité nommée est un élu ;  
- 'is_other': vrai si l'entité nommée n'est ni une association, ni un elu, mais est une personne ;
- 'total_mentions' : #docs(ent) ;   
- 'pertinence' : #{curr_doc for curr_doc in docs(ent) if themes(curr_doc)!=[]} ;      
- 'name' : le nom de l'entité nommée ;   
- 'mandates' : si l'entité nommée est un élu, ce sont ces mandats ;  
- 'themes' : set([theme for curr_doc in docs(ent) for theme in themes(curr_doc)]) ;  
- 'locations' : set([loc for curr_doc in docs(ent) for loc in locations(curr_doc)]) ;  
- 'contextes' : les extrait des documents et des metadonnées des documents dans lesquels l'entité nommée apparaît.
## Pour commencer

### Prérequis

Créer un environement virtuel et l'activer. 
```
sudo pip3 install virtualenv
virtualenv venv
source venv/bin/activate
```

run à la racine du projet :      
Pour editer l'username pour se connecter à Bitbucket :      
```
chmod +x edit_requirements.sh 
./edit_requirements.sh 
```
Pour télecharger spaCy :       
```
pip3 install -U spacy
python3 -m spacy download fr
```


Installer les requirements.    

```
pip3 install -r requirements.txt
```

Il est possible que certains requirement n'aie plus de version à jour au moment de l'installation.  
Dans ce cas :   
   - commenter la ligne du fichier requierements.txt contenant le package posant problème,  
   - activer l'environnement dans le terminal,    
   - installer la dernière version directement dans le terminal.
   
### Configuration

Créer dans config le fichier config.ini, comme une copie de  config_template.ini, puis remplacer les différents champs.   

### Données
Le code nécessite des donnée extérieures :    
**données électorales**:       
- dans path_external_data/RNE mettre les fichiers du RNE dont les noms sont les suivants : 
    "CD.txt",
    "CM 01 50.txt",
    "CM 51 OM.txt",
    'CR.txt',
    'Deputes.txt',
    'EPCI.txt',
    "groupements.csv",
    "Maires.txt",
    "Senateurs.txt"   
      
**données des associations**:      
- dans path_external_data/RNA mettre le fichier dont le nom rna_name est indiqué dans config_assos.py. Il contient les données des associations ;          

**localisation** :         
- dans input/codes_insee : mettre les codes insee des communes d'intérêt ;
- dans codes_localisation_of_interest : mettre les codes des départements et des régions d'intérêt si il y en a sinon laisser les champs vides ;
- dans list_locations_of_interest : mettre les noms des lieux d'intérêt. Les entités nommées qui sont co-occurrentes avec aucun de ses lieux sont supprimées. 
  
**topics** : remplacer les topics dans input/topics.json.     

### Config des noms des sources des documents
config/config_files/Sources_Titles_Keys.csv : les fichiers contenants les code des sources des documents. Il faut qu'il contienne les sources telles qu'elles sont 
écrite dans le champs 'source_code' des documents du corpus. Si par exemple les documents en entrée contiennent les sources 'Est_Republicain'
et 'Le Parisien', il faut que le dictionnaire des clés soit mis à jour avec ces valeurs.


exemple :

Dans DJ_Sources_Titles_Keys_v2.csv : 

```
Title,DJ Source Code,LMP Source Code
24 Heures,TFHOUR,TFHOUR
```

Pour une autre source que Dow Jones:
```
Title, Autre source,LMP Source Code
Est Républicain, Est_Republicain, ESTREP
Le Parisien, Le Parisien, PARIS

```

TFHOUR est le code de 24 heures pour la source Dow Jones. 

### Config des noms des fournisseurs de données
Les documents peuvent venir de trois fournisseurs différents :   
- "DJ" : les documents sont des .avros retournés par l'API de DJ ;  
- "Scrap" : les documents ont été scrappé ;  
- "EDD" : les documents viennent d'EDD.

Pour utiliser une autre source 'Source', il faut : 

- créer un fichier nom_du_fichier dans config, avec comme template config_dow_jones.py, avec un dictionnaire source_field_name = {nom normalisé du champs : nom du champ dans la source}. nom normalisé du champs doit être le même que celui par défaut contenu dans config_dow_jones.
 Par exemple : 'corps' : 'body', ou 'corps': 'nom_du_champs_corps_dans_la_nouvelle_source';
- de même créer un dictionnaire au format subject_codes = {"gspo" : "sport"}, {"nom_du_sujet_format_source" : "nom_du_sujet_formart_LMP"};
- dans config/config_normalisation_articles.py, depuis nom_du_fichier importer source_field_name, puis ajouter un champs 'Source' : source_field_name dans dic_to_dic_source_fields ;
- remplacer le paramètre corpus_source dans le main par 'Source' ;
- dans config/config_normalisation_articles.py, ajouter un  champs 'Source': 'Source Source Code' à dic_origin_to_source_dic_col ; 
- dans config/config_files/Sources_Titles_Keys.csv, ajouter une colone 'Source Source Code' contenant les codes des titres de presse de la nouvelle source, avec les LMP source code correpondants pour chaque ligne. 


 

### Lire le corpus en entrée

L'extraction des entités nécessite un corpus nettoyé et enrichis.     
ELle peut se faire en une étape avec :   
```
python3 main_enrich_corpus_and_extract_entities.py
```
Plusieurs sauvegardes sont effectuées au cours de cette étape.  
Le processus d'enrichissement et de selection des entités peut être repris après chacune de ces sauvegardes, en utilisant les autres 
fonctions main.
               
### Spécification des thèmes possibles pour les documents

L'enrichissement comporte une partie qui consiste à ajouter aux articles des thèmes, en fonction de lexiques définis par l'utilisateur en entrée. Ces thèmes sont à spécifier manuellement dans le fichier './input/topic.py'

### Définir la librairie de reconnaissance d'entités nommées

Par défaut, la librairie utilisée est spaCy. Pour utiliser une autre librairie, il faut : 

- créer un fichier entities_nom_de_le_librairie.py dans custom_classes, contenant une classe au même format que SpacyNer dans src/custom_classes/entities_spacy.py. SpacyNer doit avoir un attribut list_entities, et un attribut entity_type_to_entity_type.
    Chaque element de list_entities est un objet ayant les attributs suivant: 
    - .text contenant le nom de l'entité nommée;
    - .label_ contenant son type ;
    Pour l'attribut list_entities, dans config/config_ner.py, ajouter un dictionnaire au même format que spacy_entity_type_to_entity_type : {'type au format de la librairie' : entity_type['nom normalisé correspondant']}.
    L'attribut entity_type_to_entity_type est au même format que spacy_entity_type_to_entity_type dans /home/marjolaine/Documents/Influenceurs/etape_1/src/get_ner.py.
- dans src/get_ner.py, ajouter à la fonction get_entities_from_corpus le code correspondant pour la nouvelle librairie au même format que pour la librairie de google. 


## Tests

Dans le repertoire unittests se trouvent les tests. 


### Problèmes d'installations 

Lors de son installation, spaCy installe par défaut la librairie cymem qui peut être source de bugs. Si vous n'arrivez pas à éliminer le bug, supprimez le venv dans lequel vous avez installé spaCy, 
en créer un nouveau à partir d'un interpreteur python indépendant du projet, reinstallez spaCy et ensuite les requirements.

